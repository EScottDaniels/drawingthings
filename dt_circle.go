

/*
	Mnemonic:	dt_circle.go
	Abstract:	A circle drawing thing.
	Date:		20 Apr 2018 (HBD CR)
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
)

type Dt_circle struct {
	Dt_base;		// common fields and functions

	Fill_style int
	Fcolour string		// fill colour

	Radius	float64
	Line_style	int
}

// ------------------------------------------------------------------

/*
	Allow for easy programatic creation of a circle. 
*/
func Mk_circle( xo float64, yo float64, radius float64, outline_colour string, fill bool, fill_colour string ) ( *Dt_circle ){
	dt := &Dt_circle{

		Radius:	radius,
		Fcolour: fill_colour,
	}

	dt.Kind =	DT_CIRCLE			// base class things must be set outside
	dt.Colour = outline_colour
	dt.Xo =	xo
	dt.Yo =	yo
	dt.Visible = true

	if fill {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}

	return dt
}


/*
	Mk_def_circle creates  a default circle struct.
*/
func Mk_def_circle( ) ( dt *Dt_circle ) {
	dt = &Dt_circle {
		Dt_base: Dt_base {
			Kind:	DT_CIRCLE,
			Scale:	1.0,
			Visible: true,
		},
	}

	return dt
}

/*
	Draw the circle with it's current folour outline, and fill style.
	If the visible attribute for the object is false, then it is not rendered.
*/
func ( dt *Dt_circle ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil || !dt.Visible {
		return
	}
	
	if ! dt.Is_visible( scale ) {
		return
	}

	outline := false
	if dt.Colour != "none" {
		gc.Set_colour( dt.Colour )
		outline = true
	}

	pattern := Line_style_to_pattern( dt.Line_style )
	if pattern != nil {				// any line style other than solid has a pattern and must be set
		gc.Push_state()
		defer gc.Pop_state( ) 
		gc.Set_line_style( pattern )
	}

	gc.Set_fill_attrs( dt.Fcolour, dt.Fill_style )
	gc.Draw_circle( dt.Xo, dt.Yo, dt.Radius, outline )
}

/*
	Adjust the current legngth (radius) by the delta value dr
*/
func ( dt *Dt_circle ) Set_delta_length( dr float64 ) {
	if dt != nil {
		dt.Radius += dr
	}
}

/*
	Set the length (radius) to the given value r
*/
func ( dt *Dt_circle ) Set_length( r float64 ) {
	if dt != nil {
		dt.Radius = r
	}
}

/*
	Set the fill value.  If true passed, then circle will be filled.
*/
func ( dt *Dt_circle ) Set_fill(  state bool ) {
	if dt == nil {
		return
	}

	if state {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}
}

/*
	Set the fill colour to use when the circle fill state is true.
*/
func ( dt *Dt_circle ) Set_fill_colour(  cname string ) {
	if dt != nil {
		dt.Fcolour = cname
	}
}

/*
	Set_line_style allows the style of the outline to be changed
	after the thing has been created. Expects one of the *_LINE
	constants.
*/
func ( dt *Dt_circle ) Set_line_style(  style int ) {
	if dt == nil {
		return
	}

	dt.Line_style = style 
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_circle ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_circle ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) r=%.2f colour=%s filled=%v fcolour=%s vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Radius, dt.Colour, dt.Fill_style == sketch.FILL_SOLID, dt.Fcolour, dt.Visible )
	}

	return "<nil>"
}
