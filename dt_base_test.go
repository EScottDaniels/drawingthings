/*
	Testing for the base DT
*/

package drawingthings

import (
	"fmt"
	"os"
	"testing"
)

func Test_basic(t *testing.T) {
	jstr := `{ "Kind": 1, "Id": "test_base", "Colour": "#00ff00", "Xo":	100, "Yo":	300, "Rot": 0, "Visible": true  }`

	dt, err := Json2ScaledDt(jstr, 1.0)

	if err != nil {
		fmt.Fprintf(os.Stderr, "[FAIL] unable to allocate a base dt\n")
		t.Fail()
	}

	if dt == nil {
		fmt.Fprintf(os.Stderr, "[FAIL] nil pointer returned for base dt\n")
		t.Fail()
	} else {
		fmt.Fprintf(os.Stderr, "[OK]   base dt allocated: %s\n", dt)
	}

	fmt.Fprintf(os.Stderr, "[EXAM] base dt:  %s\n", dt)

/*
	nd := make(map[string]string, 17)
	nd["Kind"] = "43"    //  kind should not change
	nd["id"] = "foo bar" //  id should not change
	nd["Colour"] = "0xdeadbe"
	nd["Xo"] = "21.8"
	nd["Visible"] = "false"
	dt.Update(nd)
	fmt.Fprintf(os.Stderr, "[EXAM] base dt after update: %s\n", dt)
*/

	fmt.Fprintf(os.Stderr, "[INFO] basic test finished\n")
}
