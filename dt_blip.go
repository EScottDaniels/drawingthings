
/*
	Mnemonic:	dt_blip.go
	Abstract:	A "radar blip" which when painted will generate a triangle with one
				to three lines of text. The triangle is rotated such that it can show
				direction.  The coordinates of the blip (xo,yo) are plotted as the center
				of the triangle. If breadcrumbs is set to true, then the last 5 positions
				will be left as "dots" on the display.
	
				The data string needed to create this:
					xo:<value>
					yo:<value>
					alt:<meters>
					heading:<degrees clock from N>
					Colour:<string>
					Tcolour:<string>
					Tfill:<string>
					Raw_txt:<pipe separated fields>					
					Crumbs:<boolean>
				
	Date:		13 September 2019
	Author:		E. Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"os"
	"strings"
	"time"

	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

// data block location relative to triangle
const (
	DS_DEFAULT int = iota		// immediately right
	DS_UP_RIGHT
	DS_UP_LEFT
	DS_LOW_LEFT
	DS_LOW_RIGHT
	DS_HIDDEN					// no text; make first click for quick hide
	DS_MAX
)

const (
	BLIP_KIND_T 	int = iota	// what we draw; directional triangle
	BLIP_KIND_R					// non-directional rectangle
)

type Dt_blip struct {
	Dt_base;		// common fields and functions

	Tangle	*Dt_tangle
	Rect		*Dt_rect
	Raw_txt string		// raw text for to_* functions
	Tcolour	string		// triangle outline colour
	Tfill	string		// triangel fill colour
	Txt_size	float64		// size of the text to paint
	Heading	float64		// the rotation (degrees from 'north')
	Alt		float64		// alt in meters
	Crumbs	bool		// true if we are leaving droppings

						// private things that don't end up in json output
	kind	int			// kind of blip to paint
	text	[]*Dt_text	// text strings (colour from base)
	viz_txt	int			// number of text strings to actually paint
	ntxt	int			// number of text strings we have to possibly display
	disp_sect int;		// 'sector' that the text is written to
	droppings []*Dt_circle // droppings
	didx	int			// starting index into droppings when painting
	lastDropCap int64	// time we last captured x,y for dropping (we want to space them out)

	preserved_x		float64		// things we capture when preserved is called
	preserved_y		float64
	preserved_id	string
	preserved_kind	int
}

// ------------------------------------------------------------------

/*
	Clear the crumbs by setting their x,y off display.
*/
func (dt *Dt_blip) clear_crumbs( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil || dt.droppings == nil  || gc == nil {
		return
	}

	for i := 0; i < len( dt.droppings ); i++ {
		if dt.droppings[i] != nil { 
			dt.droppings[i].Set_origin( -1000, -1000 )
			dt.droppings[i].Paint( gc, scale )
		}
	}
}

func extract_txt( txt string ) ( []string ) {
	tokens := strings.Split( txt, "|" )
	if len( tokens ) < 1 {
		return nil
	}

	n2slice := 3
	if len( tokens ) < 3 {
		n2slice = len( tokens )
	}

	return tokens[0:n2slice]
}

/*
	Given the current blip position, compute the positon of text factoring in
	the position "sector" relative to the blip.
*/
func ( dt *Dt_blip ) get_txt_xy(  ) ( txo float64, tyo float64 ) {
	if dt == nil {
		return 0, 0
	}

	txo = dt.Xo				// starting x,y for positioning text
	tyo = dt.Yo

	switch dt.disp_sect {
		case DS_UP_RIGHT:
			tyo -= 25;

		case DS_UP_LEFT:
			tyo -=25
			txo -= 80

		case DS_LOW_RIGHT:
			tyo +=25

		case DS_LOW_LEFT:
			tyo +=25
			txo -= 80

		case DS_HIDDEN:
			txo = -1111
			tyo = -1111		// move off of display area
	}

	return txo, tyo
}

// ------------------------------------------------------------------

func blipKind() int {
	bKind := BLIP_KIND_T
	eData := os.Getenv( "DT_BLIP_KIND" )
	if eData != "" && eData[0:1] == "r" {
		bKind = BLIP_KIND_R
	}

	return bKind
}

/*
	Mk_def_blip creates  a default blip struct.
*/
func Mk_def_blip( ) ( dt *Dt_blip ) {
	bKind := blipKind()		// if set in environment, or default

	dt = &Dt_blip {
		Dt_base: Dt_base {
			Kind:		DT_BLIP,
			Scale:		1.0,
			Visible:	true,
		},

		Tcolour:	"#ffffff",
		Tfill:		"#009090",
		Tangle:		Mk_tangle( 140, 140, 10, 6, 3, true, "#ffffff", true, "#009090" ),	// x,y height, base-len, peak offset, align center, colour, fill, fill-colour
		Rect:		Mk_rect( 140, 140, 7, 7, "#ffffff", true, "#ff0000" ), // x,y height, width, clour, fill, fill colour
		Txt_size:	10,
		viz_txt:	2,
		ntxt:		0,
		Crumbs:		false,
		didx:		0,
		droppings:	make( []*Dt_circle, 7 ),
		kind:		bKind,
	}

	dt.Tangle.Set_visibility( true )
	dt.Rect.Set_visibility( true )
	dt.text = make( []*Dt_text, 3 )
	txo, tyo := dt.get_txt_xy()
	for i := 0; i < len( dt.text ); i++ {
		dt.text[i] = Mk_text( txo + 10, tyo + (float64( i ) * 10) -3, float64( 8 ), "Sans", "#909090", true, "#909090", "<unset>" )
	}

	return dt
}

/*
	Mk_blip creates a blip given the coordinates and text string. Text is assumed to be
	a single string with pipe (|) separated fields. 
*/
func Mk_blip( xo float64, yo float64, colour string, text string ) ( *Dt_blip ){
	txt := extract_txt( text )

	bKind := blipKind()		// if set in environment, or default
	dt := &Dt_blip{
		Dt_base: Dt_base {
			Xo:			xo,
			Yo:			yo,
			Kind:		DT_BLIP,
			Colour:		colour,
			Visible:	true,
		},

		Raw_txt:	text,
		Txt_size:	10,
		Tangle:		Mk_tangle( xo, yo, 10, 6, 3, true, "blue", true, "#009090" ),	// height, base-len, peak offset, align center, colour, fill, fill-colour
		Rect:		Mk_rect( xo, yo, 7, 7, "#ffffff", true, "#ff0000" ),
		ntxt:		len( txt ),
		viz_txt:	len( txt ),
		text:		make( []*Dt_text, 3 ),
		Crumbs:		false,
		didx:		0,
		droppings:	make( []*Dt_circle, 10 ),
		kind:		bKind,
	}

	txo, tyo := dt.get_txt_xy()
	for i := 0; i < dt.ntxt; i++ {
		dt.text[i] = Mk_text( txo + 10, tyo + (float64( i ) * 10) -3, float64( 8 ), "Sans", txt[i], true, colour, txt[i] )
	}

	return dt
}

/*
	Paint draws the blip (triangle and text). If we were updated, we must first update the 
	drawing things that we manage before we can paint them.

	If crumbs are on, then we will paint the breadcrumb trail (droppings) starting with
	didx and wrapping to didx-1.
*/
func ( dt *Dt_blip ) Paint( gc sketch.Graphics_api, scale float64 ) {

	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	if dt.Xo < 0 || dt.Yo < 0 {			// if moved off screen, reset text position to default
		dt.disp_sect = DS_DEFAULT
		dt.Crumbs = false
		dt.clear_crumbs( gc, scale)		//move the crumbs out of the way too
		return							// and bail now as no need to paint if out of view
	}

	x, y := dt.get_txt_xy()								// where the text goes; draw line if not in default loc first
	gc.Set_colour( dt.Colour )
	if (x != dt.Xo || y != dt.Yo) && x >= 0   {
		if( x < dt.Xo ) {
			gc.Draw_line( dt.Xo, dt.Yo, dt.Xo-20, y )
		} else {
			gc.Draw_line( dt.Xo, dt.Yo, dt.Xo+10, y )
		}
	}

	if dt.updated {
		dt.Tangle.Set_rot_angle( dt.Heading )
		dt.Tangle.Set_origin( dt.Xo, dt.Yo )
		dt.Tangle.Set_updated( true )

		dt.Rect.Set_origin( dt.Xo-3, dt.Yo-3 )
		dt.Rect.Set_updated( true )

		new_txt := extract_txt( dt.Raw_txt )
		dt.ntxt = len( new_txt )
		num2paint := dt.ntxt
		if dt.viz_txt < dt.ntxt {
			num2paint = dt.viz_txt
		}
		spacing := dt.Txt_size + 2
		for i := 0; i < num2paint; i++  {
			dt.text[i].Set_size( dt.Txt_size, 10.0 )
			dt.text[i].Set_origin( x + 10, y + ( float64( i ) * spacing) -1 )
			dt.text[i].Set_colour( dt.Colour )
			dt.text[i].Set_data( new_txt[i] )
		}
	}

	for i := 0; i < dt.viz_txt; i++ {
		dt.text[i].Paint( gc, scale )
	}

	if( dt.Crumbs ) {
		di := dt.didx
		for i := 0; i < len( dt.droppings ); i++ {
			if di >= len( dt.droppings ) {
				di = 0;
			}

			dt.droppings[di].Paint( gc, scale )
			di++
		}
	}

	if dt.kind == BLIP_KIND_T {			// paint tangle/rec last to ensure on top of crumbs
		dt.Tangle.Paint( gc, scale )
	} else {
		dt.Rect.Paint( gc, scale )
	}

	dt.Set_updated( false )
}


/*
	To_json will generate a json string from this struct. Only public fields
	make it into the json.
*/
func ( dt *Dt_blip ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

/*
	Generate a string for debugging and things related.
*/
func ( dt *Dt_blip ) string( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) colour=%s raw_text:%s  vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Colour, dt.Raw_txt, dt.Visible )
	}

	return "<nil>"
}

/*
	We accept tickles which are assumed to be mouse interactions in the form
	of a sketch/tool interaction.
*/
func (dt *Dt_blip ) Tickle( qualifier interface{} ) {
	it, ok := qualifier.( *sktools.Interaction )
	if ok {
		if it.Data == 1 {
			dt.disp_sect++
			if dt.disp_sect > DS_MAX {
				dt.disp_sect = DS_DEFAULT
			}
			dt.Set_updated( true )
		}
		return
	}

	//fmt.Fprintf( os.Stderr, "ticked! could not translate event\n" )
}

// -------------- base class overrides --------------------------------------------------

/*
	Set_origin allows the origin point to be changed effectively moving the blip
	in the drawing plane. This also captures the position in the droppings array
	and we do this even if on showing them.  updated is set to true.
*/
func (dt *Dt_blip) add_dropping( xo float64, yo float64) {
	if dt == nil {
		return
	}

	if xo < 0 || yo < 0  {
		return
	}

	if dt.droppings[dt.didx] == nil {
		dt.droppings[dt.didx] = Mk_circle( xo, yo, 1.0, "#909090", false, "#000000" )
	} else {
		dt.droppings[dt.didx].Set_origin( xo, yo )
	}

	dt.didx++
	if dt.didx  >= len( dt.droppings ) {
		dt.didx = 0
	}
}

/*
	We override preserve and restore to capture the orign values
	before an update. If they change, then we update the droppings on
	restore.
*/
func (dt *Dt_blip) Preserve() {
	if dt == nil {
		return
	}

	dt.preserved_id = dt.Id
	dt.preserved_kind = dt.Kind
	dt.preserved_x = dt.Xo
	dt.preserved_x = dt.Yo
}

/*
	If the origin changed, update the droppings.
	We also need to ensure that id and kind don't change.
*/
func (dt *Dt_blip) Restore() {
	if dt == nil {
		return
	}


	dt.Id = dt.preserved_id
	dt.Kind = dt.preserved_kind

	now := time.Now().Unix() 
	if now - dt.lastDropCap >= 5 {
		dt.lastDropCap = now
		if dt.preserved_x != dt.Xo || dt.preserved_y != dt.Yo {
			dt.add_dropping( dt.Xo, dt.Yo )
		}

		dt.updated = true
	}

}

/*
	Contains returns true if the point passed is in the triangle
	or in the bounding box of the text.
*/
func (dt *Dt_blip) Contains( x float64, y float64 ) bool {
	if dt == nil {
		return false
	}

	if dt.Tangle.Contains( x, y ) {
		return true
	}

	tx, ty := dt.get_txt_xy()
	if x > tx-5 && x < tx + 50 {	// good x
		if y > ty-5 && y < ty + 25 {
			return true
		}
	}

	return false
}
