
/*
	Mnemonic:	dt_tangle.go
	Abstract:	A triangle drawing thing. A triangle is described by three points:
					[0] lower left point on the base
					[1] lower right point on the base
					[2] peak point

				The second 'base' point, [1] is computed by adding base_len to
				the x value for [0] and rise to the y value of [0] allowing the 
				point to 'rise' above the base. 
				The peak point's y value is height units above the base (y of pont 0).
				The x value is computed by adding the peek offset to the x value of the
				first base point.

				The 'origin' of the triangle is the x,y of the first base unless Align_centre
				is true.  If Align_centre is true, then the x,y given is used as the centre 
				point of the triangle (the centroid center) is computed.
				
	Date:		27 May 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

type Dt_tangle struct {
	Dt_base;		// common fields and functions

	Fill_style int
	Fcolour string		// fill colour

	Height		float64			// amuont the peak is shifted in the y direction from yo
	Base_len	float64
	Rise		float64			// rise of the second point from the pase y
	Peak_offset float64			// amount the peak is shifted in the x direction from xo
	Align_centre	bool		// if true, the centre of the triangle is positioned at Xo,Yo.
	Adjusted		bool		// set if adjusted x,y has been set
	Adjusted_xo		float64		// adjusted lower left for quick painting
	Adjusted_yo		float64
	Line_style		int			// *_LINE consts

	Points	[]*sktools.Point	// drawing points; scaled and rotated
}

// ------------------------------------------------------------------
func ( dt *Dt_tangle ) mk_tangle_points(  ) {
	dt.Points = make( []*sktools.Point, 4 )								// points for polygon

	dt.Points[0] = sktools.Mk_point( dt.Scale * dt.Adjusted_xo, dt.Scale * dt.Adjusted_yo )
	dt.Points[1] = sktools.Mk_point( dt.Scale * (dt.Adjusted_xo + dt.Base_len), dt.Scale * ( dt.Adjusted_yo - dt.Rise ) )
	dt.Points[2] = sktools.Mk_point( dt.Scale * (dt.Adjusted_xo + dt.Peak_offset), dt.Scale * (dt.Adjusted_yo - dt.Height) )
	dt.Points[3] = dt.Points[0]
}

// ------------------------------------------------------------------


/*
	Mk_def_tangle creates  a default triangle struct.
*/
func Mk_def_tangle( ) ( dt *Dt_tangle ) {
	dt = &Dt_tangle {
		Dt_base: Dt_base {
			Kind:	DT_TANGLE,
			Scale:	1.0,
			Visible: true,
		},
	}

	return dt
}

/*
	Mk_tangle creates a triangle given the coordinates and:
		the length of the base
		the offset (x) of the peak
		the height
		the rise off of the base y for the second point

	If align center is true, then the centre of the triangle is positioned at
	Xo,Yo rather than the leftmost vertex.
*/
func Mk_tangle( xo float64, yo float64, height float64, base_len float64, peak_off float64, align_centre bool, outline_colour string, fill bool, fill_colour string ) ( *Dt_tangle ){
	dt := &Dt_tangle{
		Dt_base: Dt_base {
			Xo: xo,
			Yo: yo,
			Scale: 1.0,
			Kind:	DT_TANGLE,
			Colour: outline_colour,
			Visible: true,
		},

		Height:	height,
		Base_len:	base_len,
		Peak_offset: peak_off,
		Fcolour: fill_colour,
		Align_centre: align_centre,
		Adjusted_xo: xo,						// initially no adjustment
		Adjusted_yo: yo,
	}

	if fill {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}

	if dt.Height == 0 {
		dt.Height = 10			// must have height, but height can be negative
	}

	if align_centre {
		xc, yc := dt.Centre_xy( )	// get center points
		dt.Adjusted_xo -= ( xc - dt.Xo )
		dt.Adjusted_yo -= ( yc - dt.Yo )
	}

	return dt
}

/*
	Centre_xy computes the centroid center of the triangele; the intersection of the 
	three lines from each of the vertices to the opposite edge's midpoint.

	We do this by computing two of the lines' slopes (m1 and m2), then computing 'b'
	for the line which isn't from Xo,Yo. Given ths slopes and the y intercept of 
	the second line, we can compute the center's x value (xc) which can be plugged
	back into one of the equations to compute yc.

	The returned xc,yc are NOT ajusted based on the current Xo,Yo values, but are
	deltas:
			-add to Xo,Yo to find the center point in real coordinates
			- subtract from Xo,Yo to find the  lower left of the triangle 
			  for painting when positioning by the center.

*/
func ( dt *Dt_tangle ) Centre_xy() ( x float64, y float64 ) {
	
	m1 := (dt.Height/2) / (dt.Peak_offset + ((dt.Base_len - dt.Peak_offset)/2))
	m2 := -(dt.Height/2) / ((dt.Peak_offset/2) + (dt.Base_len-dt.Peak_offset))
	b := -m2 * dt.Base_len
	
	xc := b / (m1 - m2)
	yc :=  m1 * xc

	return xc, yc
}

/*
	Paint draws the triangle according to the information in the struct.
*/
func ( dt *Dt_tangle ) Paint( gc sketch.Graphics_api, scale float64 ) {

	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	gc.Push_state()
	defer gc.Pop_state( ) 

	outline := false
	if dt.Colour != "none" {
		gc.Set_colour( dt.Colour )
		outline = true
	}

	dt.Adjusted_xo = dt.Xo
	dt.Adjusted_yo = dt.Yo
	
	if dt.Align_centre && (dt.updated || ! dt.Adjusted) {		// initially adjusted set to xo,yo
		ax, ay := dt.Centre_xy()				// get center coordinates
		dt.Adjusted_xo = dt.Xo - ax
		dt.Adjusted_yo = dt.Yo + ay
		dt.Adjusted = true
	}

	if dt.updated || dt.Points == nil {		// need to recompute drawing points, else save effort
		dt.mk_tangle_points(  )

		if dt.Rot_angle != 0 {
			var rot_point *sktools.Point

			pt := int( dt.Rot_vertex )  
			switch pt {										// select the point to rotate round
				case 1:
					fallthrough
				case 2:
					fallthrough
				case 3:
					rot_point = dt.Points[pt-1]
			
				default:									// should be 0, but anything that isn't 1,2,3 wa'll assume centre
					if dt.Align_centre {
						rot_point = sktools.Mk_point( dt.Xo, dt.Yo )
					} else {
						rx, ry := dt.Centre_xy( ) 
						rot_point = sktools.Mk_point( rx + dt.Xo, (dt.Yo - ry) )
					}
			}

			dt.Points = Rotate_pts( rot_point, dt.Points, dt.Rot_angle )
		}
	}

	if outline {
		pattern := Line_style_to_pattern( dt.Line_style )
		if pattern != nil {				// any line style other than solid has a pattern and must be set
			gc.Set_line_style( pattern )
		}
	}

	gc.Set_fill_attrs( dt.Fcolour, dt.Fill_style )
	gc.Draw_poly( dt.Points, outline )

	dt.updated = false
}


/*
	Set_delta_hb changes the height and base length by a delta value (h and b).
*/
func ( dt *Dt_tangle ) Set_delta_hb( h float64, b float64 ) {
	if dt != nil {
		dt.Height += h
		dt.Base_len += b
	}
}

/*
	Set_size changes the current height and base length.
*/
func ( dt *Dt_tangle ) Set_size( h float64, b float64 ) {
	if dt != nil && h >= 0 && b >= 0 {
		dt.Height = h
		dt.Base_len = b
	}
}

/*
	Set_fill allows to turn on or off the fill property.
*/
func ( dt *Dt_tangle ) Set_fill(  state bool ) {
	if dt == nil {
		return
	}

	if state {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}
}

/*
	Set_fill_colour replaces the current colour with the colour passed in.
*/
func ( dt *Dt_tangle ) Set_fill_colour(  cname string ) {
	if dt != nil {
		dt.Fcolour = cname
	}
}

/*
	Set_line_style allows the style of the outline to be changed
	after the thing has been created. Expects one of the *_LINE
	constants.
*/
func ( dt *Dt_tangle ) Set_line_style(  style int ) {
	if dt == nil {
		return
	}

	dt.Line_style = style 
}

/*
	Set_rot_angle allows other objects to set the rotation angle.
*/
func ( dt *Dt_tangle ) Set_rot_angle( new_angle float64 ) {
	if dt != nil {
		dt.Rot_angle = new_angle
		dt.updated = true
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_tangle ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_tangle ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) h=%.2f b=%.2f colour=%s filled=%v fcolour=%s vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Height, dt.Base_len, dt.Colour, dt.Fill_style == sketch.FILL_SOLID, dt.Fcolour, dt.Visible )
	}

	return "<nil>"
}
