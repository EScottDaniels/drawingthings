

/*
	Mnemonic:	dt_clock.go
	Abstract:	A clock drawing thing
	Date:		8 September 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"encoding/json"
	"time"

	"bitbucket.org/EScottDaniels/sketch"
)

type Dt_clock struct {
	Dt_base;				// common fields and functions

	Font		string		// Sans, Serif, are also legit
	Size		int
	Weight		int			// stroke weight (bold-ness)
	Format		string		// GO style time format for output
	Zulu		bool
	Tz			string
	loc			*time.Location
}

// ------------------------------------------------------------------

/*
	Mk_clock allows an instance of the clock struct to be created directly rather than 
	creating a generic drawing thing.
*/
func Mk_clock( xo float64, yo float64, size float64, font string, colour string, tfmt string ) ( *Dt_clock ){
	dt := &Dt_clock{
		Dt_base: Dt_base {
			Xo: xo,
			Yo: yo,
			Kind: DT_CLOCK,
			Colour: colour,
			Visible: true,
		},
		Size:	int( size ),
		Font:	font,
		Format: tfmt,
		Tz:		"America/Detroit",
	}

	loc, err := time.LoadLocation( dt.Tz )
	if err == nil {
		dt.loc = loc
	}

	return dt
}

/*
	Mk_default_clock makes a simple clock structure onto which a json string can be 'overlaid'.
*/
func Mk_default_clock(  ) ( *Dt_clock ){
	dt := &Dt_clock{
		Dt_base: Dt_base {
			Kind: DT_CLOCK,
			Colour: "#ffffff",
			Visible: true,
		},
		Size: 12,
		Font: "Sans",
		Weight: 1,
		Format: "15:04",
		Tz:		"America/Detroit",
	}

	loc, err := time.LoadLocation( dt.Tz )
	if err == nil {
		dt.loc = loc
	}

	return dt
}

/*
	Paint draws the clock according to the information in the struct.
*/
func ( dt *Dt_clock ) Paint( gc sketch.Graphics_api, scale float64 ) {
	var( 
		now time.Time
	)

	if dt == nil || !dt.Is_visible( scale ) {
		return
	}

	if dt.loc != nil {
		now = time.Now().In( dt.loc )
	} else {
		now = time.Now()
	}

	cvalue := now.Format( dt.Format )

	gc.Set_colour( dt.Colour )
	sfs := float64( dt.Size )
	gc.Set_font( dt.Font, int( sfs ), dt.Weight, false )
	gc.Draw_text( dt.Xo, dt.Yo, cvalue )
}



/*
	h and w are delta values which are added to the current size.
	Currently delth width is ignored.
*/
func ( dt *Dt_clock ) Set_delta_hw( dsize float64, dwidth float64 ) {
	if dt != nil {
		dt.Size += int( dsize )
	}
}

/*
	Change the current size. Width is ignored at the moment.
*/
func ( dt *Dt_clock ) Set_size( size float64, width float64 ) {
	if dt != nil && size >= 0 {
		dt.Size = int( size )
	}
}


/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_clock ) To_json( ) ( string, error ) {
	return dt.String(), nil
}

func ( dt *Dt_clock ) String( ) ( string ) {
	if dt != nil {
		str, err :=  json.Marshal( dt )
		if err == nil {
			return string( str )
		}
	}

	return "{ }"
}
