
/*
	Mnemonic:	dt_meter.go
	Abstract:	A meter which consists of an arc (90, 180 or 220 degrees), direction
				clock or anticlock, needle, and readout box/text.  The meter is defined
				by specifying a center x,y and radius. Scale values are written on the 
				outside of the meter making a boundign box slightly larger than the radius.
				
	Date:		22 August 2018
	Author:		E. Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"gitlab.com/rouxware/goutils/clike"
	"bitbucket.org/EScottDaniels/sketch"
	//sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

const (
	Meter_220	int = iota
	Meter_180
	Meter_90
)

const (
	Needle_full	int = iota		// full needle line with box placed under the center point
	Needle_tic					// tic mark with box placed at center point
)

const (							// paint techniques -- smooth flow, or jump to target
	Paint_smooth	int = iota
	Paint_jump
)

type Dt_meter struct {
	Dt_base;		// common fields and functions

	Paint_style		int				// PS_ constants (smooth/jump)
	Radius			float64			// amuont the peak is shifted in the y direction from yo
	Needle_style	int
	Needle_colour	string
	Text_colour		string
	Text_size		int				// text size; if 0 we use preset values
	Arc_colour		string
	Arc_type		int				// Meter_* constants
	Tic_marks		bool
	Value_box		bool			// if false, no digital value is displayed
	Center_box		bool			// for 220/90 center if true and in needle tic. if false placed below
	Smooth_factor	float64			// number of calls required to move 10%
	Smooth_step		float64			// computed smoothing step for the current movement
	
	Val_cur			float64			// current value (min <= v <= max)
	Val_target		float64			// target value (if smooth scrolling to transition current to target)
	Val_min			float64			// value range
	Val_max			float64

	preserved_val_cur	float64		// fields preserved (not allowed to be updated)
}

// ------------------------------------------------------------------

/*
	Mk_def_meter creates a default meter struct.
*/
func Mk_default_meter( ) ( dt *Dt_meter ) {
	dt = &Dt_meter {
		Dt_base: Dt_base {
			Kind:	DT_METER,
			Scale:	1.0,
			Visible: true,
		},

		Radius: 		40,				// override things where false/0 can't be default
		Val_max:		100.0,
		Arc_colour:		"#009000",
		Text_colour:	"#00ff00",
		Needle_colour:	"#ffffff",
		Value_box:		true,
		Smooth_step:	10.0,
	}

	return dt
}

/*
	Draw_ticks adds tick marks round the arc at intervals matching pct (e.g. every 10%).
*/
func ( dt *Dt_meter ) draw_tics( gc sketch.Graphics_api, start_deg float64, end_deg float64, pct float64 ) {
	var( 
		step_deg float64
		lval	string
	)

	if dt == nil {
		return
	}

	if pct >= 1.0 {
		pct = pct/100.0
	} 
	step_deg = (end_deg - start_deg) * pct
	step_lab := (dt.Val_max - dt.Val_min) * pct
	ltic := int( (end_deg - start_deg) / step_deg ) / 4
	gc.Set_font( "Sans", 8, 0, false )		// font is already scaled; no need to do it here

	i := 0
	for deg := start_deg; deg <= end_deg; deg += step_deg {
		npt := Circle_pt( 1.0,  dt.Xo, dt.Yo, dt.Radius -1, -deg )		// compute endpoints of the tic
		tpt := Circle_pt( 1.0,  dt.Xo, dt.Yo, dt.Radius - 5.0, -deg )

		x, y := npt.Get_xy( )
		x2, y2 := tpt.Get_xy( )
		if dt.Tic_marks {
			gc.Draw_line( x, y, x2, y2 )
		}
	
		if i % ltic == 0 {
			tic_value := dt.Val_max - (step_lab * float64( i ))

			factor := 1.0
			if tic_value > 99.0 {
				factor = 2.0
			} else {
				if tic_value > 10.0 {
					factor = 1.5
				}
			}

			lx := x
			if deg > 275 {
				lx += 4.0
			} else {
				if deg > 95 {
					lx -= 4 + (8 * factor)
				} else {
					if deg > 85 {
						lx -= 4.0
					} else {
						lx += 2.0
					}
				}
			}

			ly := y
			if deg < 10 || deg > 175 {
				ly += 4.0
			} else {
				if deg >85 && deg < 95 {
					ly -= 2.0
				}
			}

			lval = fmt.Sprintf( "%.0f",  tic_value )
			gc.Draw_text( lx, ly, lval )
		}

		i++
	}
}

/*
	Paint draws the meter according to the information in the struct.
*/
func ( dt *Dt_meter ) Paint( gc sketch.Graphics_api, scale float64 ) {
	var(
		deg float64			// degrees the needle is moved from 0
		txt string			// text to write in box
	)

	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	gc.Push_state()
	defer gc.Pop_state( ) 

	if dt.Val_cur < dt.Val_target {			// adjust current when smoothing the motion
		dt.Val_cur += dt.Smooth_step
		if dt.Val_cur > dt.Val_target {
			dt.Val_cur = dt.Val_target
		}	
	} else {
		if dt.Val_cur > dt.Val_target {
			dt.Val_cur -= dt.Smooth_step
			if dt.Val_cur < dt.Val_target {
				dt.Val_cur = dt.Val_target
			}	
		}
	}

	vrange := dt.Val_max - dt.Val_min
	nv := dt.Val_cur - dt.Val_min			// normalise the value in terms of the range
	pct := nv / vrange

	font_size := 10
	if dt.Text_size > 0 {
		font_size = dt.Text_size
	} else {
		if dt.Radius >= 40 {
			font_size = 14
		}
	}

	gc.Set_line_width( 1 )
	gc.Set_colour( dt.Arc_colour )
	gc.Set_fill_attrs( "none", sketch.FILL_NONE )

	width := dt.Radius + 10.0 
	txtbox_x := float64( dt.Xo - 10.0 )		// assume 220 or 90
	txtbox_y := float64( dt.Yo + float64( font_size ) )

	switch dt.Arc_type {			// right now these are all anticlockwise
		case Meter_220:
			deg = 220 - (220 * pct)			// degrees from arc 0 which is inverse of degrees from value 0
			dt.draw_tics( gc, 0, 220, 0.10 )
			gc.Draw_arc( dt.Xo, dt.Yo, dt.Radius, 0.0, 220.0 )
			width = dt.Radius + 20.0

			if dt.Center_box && dt.Needle_style == Needle_tic {
				txtbox_x = float64( dt.Xo - dt.Radius/2.0-5 )
				txtbox_y = float64( dt.Yo - dt.Radius/2.0 )
				width = dt.Radius + 10.0
			}

		case Meter_180:
			deg = 180 - (180 * pct)
			dt.draw_tics( gc, 0, 180, 0.20 )
			gc.Draw_arc( dt.Xo, dt.Yo, dt.Radius, 0.0, 180.0 )
			txtbox_x = float64( dt.Xo - dt.Radius/2.0-5 )			// box always centered here
			txtbox_y = float64( dt.Yo - dt.Radius/3.0 )

		case Meter_90:
			txtbox_x += 15
			deg = 90 - (90 * pct)
			dt.draw_tics( gc, 0, 90, 0.25 )
			gc.Draw_arc( dt.Xo, dt.Yo, dt.Radius, 0.0, 90.0 )
			if dt.Center_box && dt.Needle_style == Needle_tic {
				width = dt.Radius
				txtbox_x = float64( dt.Xo - dt.Radius/2.0-5 )
				txtbox_y = float64( dt.Yo - dt.Radius/2.0 )
			}
	}

	if dt.Value_box {
		ffs := float64( font_size )
		gc.Set_font( "Sans", int( ffs ), 0, false )
		txt_x := txtbox_x + 3												// text location inside of box
		txt_y := (txtbox_y + float64( font_size )) + 3.0
		gc.Draw_rect( txtbox_x, txtbox_y - 2,  float64( font_size * 2 ), width, true )
		if dt.Val_cur < 100 {
			txt = fmt.Sprintf( "%.2f", dt.Val_cur )
		}  else {
			if dt.Val_cur < 1000 {
				txt = fmt.Sprintf( "%.1f", dt.Val_cur )
			} else {
				txt = fmt.Sprintf( "%.0f", dt.Val_cur )
			}
		}
		gc.Set_colour( dt.Text_colour )
		gc.Draw_text( txt_x, txt_y, txt )
	}

	gc.Set_colour( dt.Needle_colour )
	if dt.Needle_style == Needle_full {
		gc.Set_line_width( 2 )
		npt := Circle_pt( 1.0,  dt.Xo, dt.Yo, dt.Radius, -deg )			// point for other end of needle
		x, y := npt.Get_xy( )
		gc.Draw_line( dt.Xo, dt.Yo, x, y )
	} else {
		gc.Set_line_width( 3 )
		npt := Circle_pt( 1.0,  dt.Xo, dt.Yo, dt.Radius+2, -deg )			// stub extends past the arc by a bit
		tpt := Circle_pt( 1.0,  dt.Xo, dt.Yo, dt.Radius - 5.0, -deg )		// used in place of center
		x, y := npt.Get_xy( )
		x2, y2 := tpt.Get_xy( ) 				// the point of the 'stub'
		gc.Draw_line( x, y, x2, y2 )
	}
	gc.Set_line_width( 1 )

	dt.updated = false
}

/*
	Preerve saves the fields which may not be updated/overlaid. The generic
	Json_update() function in drawingthings.go will invoke this before overlaying
	the struct with caller json, and then will invoke retore() to restore the 
	preserved values. This funciton is needed as the current value must be preserved
	when smooth adjusting the meter.
*/
func ( dt *Dt_meter ) Preserve( ) {
	if dt == nil {
		return
	}

	dt.preserved_id = dt.Id
	dt.preserved_kind = dt.Kind
	dt.preserved_val_cur = dt.Val_cur
}

/*
	Restore copies presrved values back into the main struct. This allows us to 
	protect some fields from being overlaid with user data. Id, Kind and Val_cur
	fields are preserved/restored.
*/
func ( dt *Dt_meter ) Restore( ) {
	if dt == nil {
		return
	}

	dt.Id = dt.preserved_id
	dt.Kind = dt.preserved_kind
	dt.Val_cur = dt.preserved_val_cur

}

/*
	Set_data sets the target value for the meter. If jump positioning
	is selected for the meter, then the needle will be shown in this 
	position on the next paint. If smooth needle positioning is selected,
	then the needle will be moved in steps between the current position to
	the supplied target position.  Target will be constrained between the 
	current meter max and min values.
*/
func ( dt *Dt_meter ) Set_data( data_thing interface{} ) {
	var (
		target float64
	)

	if dt == nil {
		return
	}

	switch d := data_thing.(type) {
		case float64:
			target = d

		case int:
			target = float64( d )

		case string:
			target = clike.Atof( d )

		case *string:
			target = clike.Atof( d )
	}

	if target > dt.Val_max {
		dt.Val_target = dt.Val_max
	} else {
		if target < dt.Val_min {
			dt.Val_target = dt.Val_min
		} else {
			dt.Val_target = target
		}
	}
}

func ( dt *Dt_meter ) Tickle( idata interface{} ) {
	if dt == nil {
		return
	}

	data := ""
	switch dt := idata.( type ) {
		case string:
			data = dt

		case *string:
			data = *dt

		default:
			return;
	}

	switch data {
		case "zero":
			dt.Val_target = dt.Val_min

		case "max":
			dt.Val_target = dt.Val_max
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_meter ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

/*
func ( dt *Dt_meter ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) h=%.2f b=%.2f colour=%s filled=%v fcolour=%s vis=%v", 

			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Height, dt.Base_len, dt.Colour, dt.Fill_style == sketch.FILL_SOLID, dt.Fcolour, dt.Visible )
	}

	return "<nil>"
}
*/
