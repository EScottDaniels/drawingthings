
/*
	Mnemonic:	dt_text.go
	Abstract:	A text drawing thing.
	Date:		2 Jun 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"strings"
	"encoding/json"

	"bitbucket.org/EScottDaniels/sketch"
)

type Dt_text struct {
	Dt_base;				// common fields and functions

	Text		string		// the 'data' to display
	Font		string		// Sans, Serif, are also legit
	Size		int
	Weight		int			// stroke weight (bold-ness)
	Ital		bool
	Fill_style	int			// ability to fill is a future thing
	Fcolour		string		// fill colour
}

// ------------------------------------------------------------------

/*
	Mk_text allows an instance of the text struct to be created directly rather than 
	creating a generic drawing thing.
*/
func Mk_text( xo float64, yo float64, size float64, font string, outline_colour string, fill bool, fill_colour string, text string ) ( *Dt_text ){
	dt := &Dt_text{
		Dt_base: Dt_base {
			Xo: xo,
			Yo: yo,
			Kind: DT_TEXT,
			Colour: outline_colour,
			Visible: true,
		},
		Size:	int( size ),
		Font:	font,
		Fcolour: fill_colour,
	}

	if fill {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}

	return dt
}

/*
	Paint draws the text according to the information in the struct.
*/
func ( dt *Dt_text ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	if dt.Rot_angle != 0 {
		gc.Push_state( )
		defer gc.Pop_state( )							// ensure state popped before return
		gc.Translate_delta( dt.Xo, dt.Yo )				// rotate round the anchor point
		gc.Rotate( dt.Rot_angle )
		gc.Translate_delta( -dt.Xo, -dt.Yo )					// translate back so that writing at x,y works 
	}

	gc.Set_colour( dt.Colour )
	ffs := float64( dt.Size )
	gc.Set_font( dt.Font, int( ffs ), dt.Weight, dt.Ital )
	gc.Draw_text( dt.Xo, dt.Yo, dt.Text )
}

/*
	Set_data allows the text, and text colour,  associated with this dt to be changed.
	A string is accepted, and if it begins with "colourxxxxxx," then the characters up to 
	the comma are used to set the colour. The xxxx can be a colour name or a hex rgb 
	value if proceeded with a hash.
*/
func ( dt *Dt_text ) Set_data( data_thing interface{} ) {
	var (
		new_txt string
	)

	if dt == nil {
		return
	}

	switch d := data_thing.(type) {
		case float64:
			new_txt = fmt.Sprintf( "%.2f", d )

		case int:
			new_txt = fmt.Sprintf( "%d.0", d )

		case string:
			new_txt = d

		case *string:
			new_txt = *d
	}

	if len( new_txt ) > 7 && new_txt[0:7] == "colour=" {
		tokens := strings.SplitN( new_txt, ",", 2 )
		dt.Colour = tokens[0][7:]
		if len(tokens) < 2 || tokens[1] == "" {
			return
		}

		new_txt = tokens[1]
	}

	dt.Text = new_txt
}


/*
	h and w are delta values which are added to the current size.
	Currently delth width is ignored.
*/
func ( dt *Dt_text ) Set_delta_hw( dsize float64, dwidth float64 ) {
	if dt != nil {
		dt.Size += int( dsize )
	}
}

/*
	Change the current size. Width is ignored at the moment.
*/
func ( dt *Dt_text ) Set_size( size float64, width float64 ) {
	if dt != nil && size >= 0 {
		dt.Size = int( size )
	}
}

/*
	Set the fill value.  If true passed, then text will be filled
*/
func ( dt *Dt_text ) Set_fill(  state bool ) {
	if dt == nil {
		return
	}

	if state {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}
}

/*
	Set the fill colour to use when the text fill state is true.
*/
func ( dt *Dt_text ) Set_fill_colour(  cname string ) {
	if dt != nil {
		dt.Fcolour = cname
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_text ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_text ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) size=%.d colour=%s vis=%v, text=%s", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Size, dt.Colour, dt.Visible, dt.Text )
	}

	return "<nil>"
}
