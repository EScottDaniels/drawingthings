

/*
	Mnemonic:	drawingthing.go
	Abstract:	The base interface for all drawing things. 

				A note about scale:  Scale is passed to Paint() calls such that the thing
				can be hidden when the scale is too fine or coarse, but scale will NOT affect
				the actual size or placement of the thing when rendered.  This wasn't always the
				case, but as the underlying sketch X interface now supports scale, it not 
				necessary to implement scale factoring in drawing things. Except... for drawing
				things which need subpages (e.g. graphs). The subpages must be scaled when created
				and are not dynamically changed after creation.

	Date:		20 Apr 2018 (HBD CR)
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"

	"bitbucket.org/EScottDaniels/data_mgr"
	"bitbucket.org/EScottDaniels/sketch"
)

const (
	DT_BASE	int = iota + 1		// start at 1 so if kind missing from json, the reduling 0 doesn't bugger us up
	DT_RECT
	DT_LINE
	DT_TEXT
	DT_ARC
	DT_POLY
	DT_CIRCLE
	DT_PIECHART
	DT_GRAPH
	DT_BARGRAPH
	DT_RUNGRAPH
	DT_TANGLE
	DT_REGPOLY
	DT_METER
	DT_CLOCK
	DT_IMAGE
	DT_BLIP
)

type dt_kind int

/*
	The drawing thing interface.
	All drawingthings must implement these functions.
*/
type Drawingthing_i interface {
	Contains( x float64, y float64 ) ( bool )
	Get_data() ( *data_mgr.Data_group )
	Get_id() ( string )
	Get_text() ( string )
	Is_kind( kind int ) ( bool )
	//Is_updated() || Needs_paint() || Is_animated()
	Nuke( sketch.Graphics_api )
	Paint( gc sketch.Graphics_api, scale float64 )
	Preserve()
	Restore()
	Rotate(	vertex float64, angle float64 )
	Set_colour( cname string ) 
	Set_data( data interface{} )
	Set_delta_hw( dheight float64, dwidth float64 )
	Set_delta_length( dlength float64 )
	Set_endpt( x1 float64, y1 float64 )
	Set_fill( state bool )
	Set_fill_colour( cname string )
	Set_hw( height float64, width float64 )
	Set_length( length float64 )
	Set_line_style( style int )
	Set_iscale( new_scale float64 )
	Set_scale( new_scale float64 )
	Set_origin( new_xo float64, new_yo float64 )
	Set_rel_origin( delta_xo float64, delta_yo float64 )
	Set_vis_threshold( new_threshold float64 )
	String( ) ( string )
	Tickle( data interface{} )
	To_json( ) ( string, error )
}

/*
	Basic struct with ONLY the fields needed to determine kind.
*/
type J2dt struct {
	Kind	int
}


/*
	something_to_bytes accepts stirng, *string, or even []byte, and 
	converts it to a slice of bytes.
*/
func something_to_bytes( something interface{} ) ( []byte ) {
	switch x := something.( type ) {
		case string:
			return []byte( x )

		case *string:
			return []byte( *x )

		case []byte:
			return x

		default:
			return nil
	}
}


/*
	Old proto to keep this change from being a breaking change.
	Deprecated; use Json2ScaledDt( jif, scale )
*/
func Json2dt( jif interface{} ) ( Drawingthing_i,  error ) {
	return Json2ScaledDt( jif, 1.0 )
}

/*
	Given a json description of a drawing thing this function unpacks the json to
	determine the type, then creates the proper struct from the json. Finally
	it returns a pointer to the interface which then can be used as follows:

		dt, err = Json2ScaledDt( json_string, scale )
		if err == nil {
			dt.Set_endpt( x, y )
		}

	The type of drawing thing is determined by the Kind field in the json which must
	be one of the DT_* constants.  The struct is initialised with a call to the 
	MK_default_*() function, and then the json is 'applied' over top; thus the json
	needs to supply only the values which override the defaults. For simple things, 
	defaults are likely just all 0s, where as things like graphs the defaults are
	probalby much more complicated.

*/
func Json2ScaledDt( jif interface{}, scale float64 ) ( Drawingthing_i,  error ) {
	var (
		jbytes	[]byte
	)

	jbytes = something_to_bytes( jif )
	if jbytes == nil {
		return nil, fmt.Errorf( "unrecognised jif type" )
	}
	
	jdt := new( J2dt )
	err := json.Unmarshal( jbytes, &jdt )
	if err == nil {
		if jdt.Kind > 0 {
			switch( jdt.Kind ) {
				case DT_BASE:							// really only for testing
					dt := new( Dt_base )
					dt.Visible = true
					err = json.Unmarshal( jbytes, dt )
					return  dt, err

				case DT_RECT:
					dt := new( Dt_rect )
					dt.Visible = true
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_LINE:
					dt := new( Dt_line )
					dt.Visible = true
					err = json.Unmarshal( jbytes, &dt )
					return dt, err

				case DT_TEXT:
					dt := new( Dt_text )
					dt.Visible = true
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_TANGLE:
					dt := Mk_def_tangle( )
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_CIRCLE:
					dt := Mk_def_circle( )
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_REGPOLY:
					dt := Mk_def_poly( )
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_BARGRAPH:
					dt := Mk_default_bar_graph( )			// build the default values
					err = json.Unmarshal( jbytes, &dt )		// apply overrides from the json
					if err == nil {
						dt.Set_scale( scale )
					}
					return dt, err

				case DT_RUNGRAPH:
					dt := Mk_default_run_graph( )			// build struct with defaults
					err = json.Unmarshal( jbytes, &dt )		// override with the json passed in
					if err == nil {
						dt.Set_scale( scale )
					}
					return dt, err

				case DT_METER:
					dt := Mk_default_meter( ) 
					err = json.Unmarshal( jbytes, &dt )
					return dt, err

				case DT_BLIP:
					dt := Mk_def_blip( )
					dt.Set_updated( true )
					err = json.Unmarshal( jbytes, &dt )
					return  dt, err

				case DT_CLOCK:
					dt := Mk_default_clock()
					err = json.Unmarshal( jbytes, &dt )
					return dt, err

				case DT_IMAGE:
					dt := Mk_default_image()
					err = json.Unmarshal( jbytes,&dt )
					dt.Fix_fnames() 						// split any multiple filenames
					return dt, err
	
				default:
					return nil, fmt.Errorf( "unrecognised drawingthing type: %d", jdt.Kind )
			}
		}
	}

	return nil, err
}

/*
	Json_update allows a set of updated values to be applied to an object using a json
	blob containing the fields that should be updated. Any field not contained in the 
	blob should remain as it exists. The specific drawing type may not allow some fields
	(like the id) to be updated after creation, thus supplying them will have no effect.
*/
func Json_update( dt Drawingthing_i, jblob interface{} ) {
	jbytes := something_to_bytes( jblob ) 		// marshall only accepts bytes; convert

	dt.Preserve()								// they're about to be reamed, allows for stashing things that shouldn't change
	err := json.Unmarshal( jbytes, dt )				// overlay the struct with the changes
	if err != nil {
		fmt.Printf( ">>> unable to update dt with '%s': %s\n", string( jbytes ), err )
	}

	dt.Restore()								// allows restoration of preserved things
}

