
/*
	Testing for the various shape things
*/

package drawingthings

import (
	"fmt"
	"os"
	"testing"
)

func Test_rect( t *testing.T )  {
	jstr := fmt.Sprintf( `{ "Kind": %d, "Id": "test_base", "Colour": "#00ff00", "Xo":	100, "Yo":	300, "Rot": 0, "Height":  10, "Width": 10, "Filled": false, "Fcolour": "#000001", "Visible": true }`, 
		DT_RECT )

	dt,err := Json2ScaledDt( jstr, 1.0 )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] unable to create dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] returned pointer was nil!\n" )
		t.Fail()
		return
	}

	dt.Set_endpt( 103.7, 94.5 )							// this shouuld have no effect since rect does not support it
	fmt.Fprintf( os.Stderr, "[OK]   created dt from base json: %s\n", dt ) 

	fmt.Fprintf( os.Stderr, "[INFO] rect test finished\n" )
}

func Test_line( t *testing.T )  {
	jstr := fmt.Sprintf( 
		`{ "Kind": %d, "Id": "test_base", "Colour": "#00ff00", "Xo":	100, "Yo":	300, "Rot": 0, "Height":  10, "Width": 10, "Filled": false, "Fcolour": "#000001", "Visible": true }`, 
		DT_LINE )

	dt, err := Json2ScaledDt( jstr, 1.0 )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] unable to create dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] returned pointer was nil!\n" )
		t.Fail()
		return
	}

	dt.Set_endpt( 103.7, 94.5 )
	fmt.Fprintf( os.Stderr, "[OK]   created dt from base json: %s\n", dt ) 

	/*
	nd := make( map[string]string, 17 ) 
	nd["Kind"] = "43"							// kind should not change
	nd["id"] = "foo bar"						// id should not change
	nd["Colour"] = "0xdeadbe"
	nd["Y1"] = "21.8"
	nd["X1"] = "21.8"
	nd["Visible"] = "false"
	dt.Update( nd )
	fmt.Fprintf( os.Stderr, "[EXAM] base dt after update: %s\n", dt )
	*/

	fmt.Fprintf( os.Stderr, "[INFO] rect test finished\n" )
}
 
