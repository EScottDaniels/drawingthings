
/*
	Mnemonic:	dt_line.go
	Abstract:	A line drawing thing.
	Date:		21 Apr 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"

	"bitbucket.org/EScottDaniels/sketch"
)


type Dt_line struct {
	Dt_base;			// common fields and functions

	X1	float64
	Y1	float64

	Line_style	int
}

/*
	Mk_dt_line creates a line with the given end points and returns a pointer
	to the structure.
*/
func Mk_line( x0 float64, y0 float64, x1 float64, y1 float64, colour string, style int ) ( *Dt_line ) {
	dt := &Dt_line {
		Dt_base: Dt_base {
			Xo: x0,
			Yo: y0,
			Colour: colour,
			Visible: true,
		},

		X1: x1,
		Y1: y1,
		Line_style: style,
	}

	return dt
}

/*
	Given a graphics context, do what is needed to draw the line.
*/
func ( dt *Dt_line ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil {
		return
	}

	if ! dt.Is_visible( scale ) {
		return
	}

	pattern := Line_style_to_pattern( dt.Line_style )

	gc.Set_colour( dt.Colour )	// must set early so that it remains the current colour after pop
	if pattern != nil  {
		gc.Push_state( )
		defer gc.Pop_state( )
		gc.Set_line_style( pattern )
	}

	gc.Draw_line( dt.Xo, dt.Yo, dt.X1, dt.Y1 )
}

func ( dt *Dt_line ) Set_endpt( x1 float64, y1 float64 ) {
	if dt != nil  {
		dt.X1 = x1
		dt.Y1 = y1
	}
}

func ( dt *Dt_line ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) endpt=(%.2f,%.2f) colour=%s  vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.X1, dt.Y1, dt.Colour, dt.Visible )
	}

	return "<nil>"
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_line ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

/*
	Given a map (nd - new data) of key/value pairs, update the fields that we find in the map.
	Returns true if any values were changed; false if the data was not altered.
func ( dt *Dt_line ) Update( nd map[string]string ) ( bool ){
	if dt == nil {
		return false
	}

	dt.updated = false
	// id and kind may NOT be changed even if in the map

	dt.Xo = dt.update_value( nd, "Xo", dt.Xo ).( float64 )
	dt.Yo = dt.update_value( nd, "Yo", dt.Yo ).( float64 )
	dt.X1 = dt.update_value( nd, "X1", dt.Xo ).( float64 )
	dt.Y1 = dt.update_value( nd, "Y1", dt.Yo ).( float64 )

	dt.Colour = dt.update_value( nd, "Colour", dt.Colour ).( string  ) 
	dt.Visible = dt.update_value( nd, "Visible", dt.Visible ).( bool )

	return dt.updated
}
*/
