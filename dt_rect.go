/*
	Mnemonic:	dt_rect.go
	Abstract:	A rectangle drawing thing.
	Date:		20 Apr 2018 (HBD CR)
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

type Dt_rect struct {
	Dt_base;		// common fields and functions

	Fill_style int
	Fcolour string		// fill colour

	Height	float64
	Width	float64
	Line_style int
}

// ------------------------------------------------------------------

/*
	Mk_rect allows an instance of the rect struct to be created directly rather than 
	creating a generic drawing thing.  Xo,Yo is upper left corner.
*/
func Mk_rect( xo float64, yo float64, height float64, width float64, outline_colour string, fill bool, fill_colour string ) ( *Dt_rect ){
	dtr := &Dt_rect{
		Dt_base: Dt_base {
			Kind:	DT_RECT,
			Xo: xo,
			Yo: yo,
			Colour: outline_colour,
			Visible: true,
		},

		Height:	height,
		Width:	width,
		Fcolour: fill_colour,
	}

	if fill {
		dtr.Fill_style = sketch.FILL_SOLID
	} else {
		dtr.Fill_style = sketch.FILL_NONE
	}

	return dtr
}

/*
	Paint draws the rectangle according to the information in the struct.
*/
func ( dt *Dt_rect ) Paint( gc sketch.Graphics_api, scale float64 ) {
	var( 
		rot_point *sktools.Point
	)

	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	if dt.Rot_angle != 0 {
		pt := int( dt.Rot_vertex )  		// which corner should we rotate round?
		switch pt {										// select the point to rotate round
			case 1:
				rot_point = sktools.Mk_point( dt.Xo, dt.Yo )							// lower left
				
			case 2:
				rot_point = sktools.Mk_point( dt.Xo + dt.Width, dt.Yo )					// lower right

			case 3:
				rot_point = sktools.Mk_point( dt.Xo + dt.Width, dt.Yo + dt.Height )		// upper right

			case 4:
				rot_point = sktools.Mk_point( dt.Xo, dt.Yo + dt.Height )				// upper left
		
			default:							// should be 0, but anything that isn't 1,2,3, or 4 we'll assume centre
				rot_point = sktools.Mk_point( dt.Xo + (dt.Height/2), dt.Yo + (dt.Height/2) )
		}

		gc.Push_state( )
		defer gc.Pop_state( )							// ensure state popped before return
		rx, ry := rot_point.Get_xy( )
		gc.Translate_delta( rx, ry )
		gc.Rotate( dt.Rot_angle )
		gc.Translate_delta( -rx, -ry )
	}

	pattern := Line_style_to_pattern( dt.Line_style )
	if pattern != nil {				// any line style other than solid has a pattern and must be set
		if dt.Rot_angle == 0 {		// if not pushed earlier
			gc.Push_state()
			defer gc.Pop_state( ) 
		}	
		gc.Set_line_style( pattern )
	}

	outline := false
	if dt.Colour != "none" {
		gc.Set_colour( dt.Colour )
		outline = true
	}

	gc.Set_fill_attrs( dt.Fcolour, dt.Fill_style )
	gc.Draw_rect( dt.Xo, dt.Yo, dt.Height, dt.Width, outline )
}

/*
	h and w are delta values which are added to the current size.
*/
func ( dt *Dt_rect ) Set_delta_hw( h float64, w float64 ) {
	if dt != nil {
		dt.Height += h
		dt.Width += w
	}
}

/*
	Set_line_style allows the style of the outline to be changed
	after the thing has been created. Expects one of the *_LINE
	constants.
*/
func ( dt *Dt_rect ) Set_line_style(  style int ) {
	if dt == nil {
		return
	}

	dt.Line_style = style 
}

/*
	Change the current height and width.
*/
func ( dt *Dt_rect ) Set_size( h float64, w float64 ) {
	if dt != nil && h >= 0 && w >= 0 {
		dt.Height = h
		dt.Width = w
	}
}

/*
	Set the fill value.  If true passed, then rectangle will be filled
*/
func ( dt *Dt_rect ) Set_fill(  state bool ) {
	if dt == nil {
		return
	}

	if state {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}
}

/*
	Set the fill colour to use when the rectangle fill state is true.
*/
func ( dt *Dt_rect ) Set_fill_colour(  cname string ) {
	if dt != nil {
		dt.Fcolour = cname
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_rect ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_rect ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) h=%.2f w=%.2f colour=%s filled=%v fcolour=%s vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Height, dt.Width, dt.Colour, dt.Fill_style == sketch.FILL_SOLID, dt.Fcolour, dt.Visible )
	}

	return "<nil>"
}
