
/*
	Abstract:	Various tools that all DTs use
	Date:		21 April 2018
	Author: 	E. Scott Daniels
*/

package drawingthings

import (
	"math"

	"gitlab.com/rouxware/goutils/clike"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

const( 
	deg2rad	float64 = 0.0174533
) 

const (						// line style constants
	SOLID_LINE	int = iota
	DASHED_LINE
	DOT_LINE
	DOT_DOT_DASH_LINE
	DOT_DASH_LINE
)

/*
const (						// string constants that can be used in json for line styles
	DT_LSTR_DASHED				string = "[4, 2]"
	DT_LSTR_DOT					string = "[2, 2 ]"
	DT_LSTR_DOT_DOT_DASH		string = "[ 2, 2, 2, 2, 4, 2 ]"
	DT_LSTR_DOT_DASH			string = "[ 2, 2, 4, 2 ]"
)
*/

/*
	Given a string and type, convert the string and return it. If we don't recognise the 
	type from the default_value, or don't support it, we return the default value.
*/
func str2value( str string, def_val interface{} ) ( interface{} ) {

	switch def_val.( type ) {					// based on the type from our struct, convert value
		case string:
			return str

		case *string:
			return &str

		case float64:
			fv := clike.Atof( str )				// convert in a C like manner; stopping at first non digit/sign/decimal
			return fv

		case int:
			iv := clike.Atoi( str )
			return int( iv )

		case bool:
			return (str == "true") || (str == "True")
	}

	return def_val
}

/*
	Compute the point (x,y) on a circle assuming a center of cx,cy, angle formed by a straight line
	running from cx,cy to the point,  and radius r. Scale is applied to the x,y and radius values
*/
func Circle_pt( scale float64, cx float64, cy float64, radius float64, angle float64 ) ( *sktools.Point ) {
	x := math.Cos( angle * deg2rad ) * radius
	y := math.Sin( angle * deg2rad ) * radius

	return sktools.Mk_point( scale * (x + cx), scale * (y + cy) )
}

func Rotate_pts( rpt *sktools.Point, points []*sktools.Point, angle float64 ) ( new_points []*sktools.Point ) {
	new_points = make( []*sktools.Point, len( points ) )
	
	cx, cy := rpt.Get_xy( )		// get the centre of rotation
	for i, p := range points {
		ox, oy := p.Get_xy()

		if ox == cx && oy == cy {
			new_points[i] = sktools.Mk_point( ox, oy )
		} else {
			r360 := 360 * deg2rad
			r180 := 180 * deg2rad

			x := ox - cx
			y := oy - cy
			r := math.Sqrt( (x * x) + (y * y) )

			a := float64( 0.0 )				// compute angle to current point
			ra := angle * deg2rad			// rotaional angle in radians
			if x >= 0 && y >= 0  {			// figure the new angle based on quadrant current  point in relative to center
				a = math.Asin(  y/r )
			} else {
					if x < 0 && y >= 0 {
					a = r180 - math.Asin( y/r )	
				} else {
					if x < 0 && y < 0  {
						a = r180 + math.Asin( math.Abs(y)/r )
					} else {
						if x >= 0 && y < 0 {
							a = r360 - math.Asin( math.Abs(y)/r )
						}
					}
				}
			}
			
			a += ra
			if a >= (360.0 * deg2rad) {
				a -= 360.0 * deg2rad
			}
			nx := cx + (math.Cos( a ) * r)
			ny := cy + (math.Sin( a ) * r)
			//fmt.Printf( ">>> (%.2f, %.2f) r=%.2f %.2f\n\n", nx, ny, r, a/deg2rad )

			new_points[i] = sktools.Mk_point( nx, ny )
		}
	}

	return new_points
}



/*
	Line_style_to_pattern accepts a DT_*_LINE constant and returns an integer pattern
	that can be used by the underlying graphics construct to set the line drawing 
	style.
*/
func Line_style_to_pattern( style int ) ( []int ) {
	
	switch style {
		case DASHED_LINE:
			return  []int{ 4, 2 }		// 4 on 2 off

		case DOT_LINE:
			return []int{ 2, 2 }		// 2 on 2 off
			
		case DOT_DOT_DASH_LINE:
			return  []int{ 2, 2, 2, 2, 4, 2 }		// 2 0n, 2 off, 2 on 2 off, 4 on 2 off

		case DOT_DASH_LINE:
			return []int{ 2, 2,  4, 2 }		// 2 0n, 2 off, 4 on 2 off
	}

	return nil
}
