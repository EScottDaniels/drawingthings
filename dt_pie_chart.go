

/*
	Mnemonic:	dt_pie_chart.go
	Abstract:	Draw a pie chart at a given point (origin) with a given radius, an array of percentages, 
				and an array of colours.
	Date:		20 Apr 2018 (HBD CR)
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
)

type Dt_piechart struct {
	Dt_base					// common fields and functions

	Sep_colour string		// the colour to use to separate pieces of the pie
	Fill_style int
	Fcolours []string		// fill colours, one per slice
	Pcts	[]float64		// percentages, one per slice

	Radius	float64
}

// ------------------------------------------------------------------

/*
	Programatic creation of a pie chart. The colours an percentages can 
	be a single element array, and added to over the lifespan of the object.
*/
func Mk_piechart( xo float64, yo float64, radius float64, outline_colour string, sep_colour string, pcts []float64, fillcolours []string  ) ( *Dt_piechart ) {
	dt := &Dt_piechart{
		Radius:	radius,
		Pcts: pcts,
		Fcolours: fillcolours,
		Sep_colour: sep_colour,
	}

	dt.Xo = xo
	dt.Yo = yo
	dt.Kind =	DT_PIECHART			// base class things must be set outside
	dt.Colour = outline_colour
	dt.Visible = true

	dt.Fill_style = sketch.FILL_SOLID

	return dt
}

/*
	Create a pie chart given an array of values which are normalised into percentages of the overall total.
	Fill colours can be supplied on the call, or added later.
*/
func Mk_piechart_values( xo float64, yo float64, radius float64, outline_colour string, sep_colour string, values []float64, fillcolours []string  ) ( *Dt_piechart ){
	dt := &Dt_piechart{
		Radius:	radius,
		Fcolours: fillcolours,
		Sep_colour: sep_colour,
	}

	dt.Xo = xo
	dt.Yo = yo
	dt.Kind =	DT_PIECHART			// base class things must be set outside
	dt.Colour = outline_colour
	dt.Visible = true

	dt.Fill_style = sketch.FILL_SOLID

	total := float64( 0 )
	for _, v := range( values ) {
		total += v
	}

	if total == 0 {
		total = 1
	}

	dt.Pcts = make( []float64, len( values ) )
	for i, v := range( values ) {
		dt.Pcts[i] = v/total
	}

	return dt
}

/*
	Add a percentage value to the array.
*/
func ( dt *Dt_piechart ) Add_pctg( p float64 ) {
	if dt == nil {
		return
	}

	if dt.Pcts == nil {
		dt.Pcts = make( []float64, 0, 10 )
	}

	dt.Pcts = append( dt.Pcts, p )
}

/*
	Add a fill colour  value to the array. Cname may be a supported colour
	name (e.e. yellow) or a hex rgb value of the form #rrggbb or 0xrrggbb.
*/
func ( dt *Dt_piechart ) Add_colour( cname string ) {
	if dt == nil {
		return
	}

	if dt.Fcolours == nil {
		dt.Fcolours = make( []string, 0, 10 )
	}

	dt.Fcolours = append( dt.Fcolours, cname )
}

/*
	Draw the chart with it's current folours, and  outline.
	If the visible attribute for the object is false, then it is not rendered.
*/
func ( dt *Dt_piechart ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	gc.Set_colour( dt.Sep_colour )					// switch to seperator colour for outline
	start_a := float64( 0 )			// angle 'end points' in degrees
	end_a := float64( 0 )
	for i, v := range( dt.Pcts ) {
		end_a += 360.0 * v
		gc.Set_fill_attrs( dt.Fcolours[i], sketch.FILL_SOLID )
		gc.Draw_pie( dt.Xo, dt.Yo, dt.Radius, start_a, end_a, true )

		start_a = end_a
	}

	if dt.Colour != "none" {
		gc.Set_colour( dt.Colour )
		gc.Set_fill_attrs( "white", sketch.FILL_NONE )
		gc.Draw_circle( dt.Xo, dt.Yo, dt.Radius, true )
	}
}

/*
	Clear the values and optionally the colours.
*/
func ( dt *Dt_piechart ) Clear_values( clear_colours bool  ) {
	if dt == nil || !dt.Visible {
		return
	}

	dt.Pcts = nil
	if clear_colours {
		dt.Fcolours = nil
	}
}


/*
	Adjust the current radius by the delta value dr
*/
func ( dt *Dt_piechart ) Set_delta_radius( dr float64 ) {
	if dt != nil {
		dt.Radius += dr
	}
}

/*
	Set the radius to the given value r
*/
func ( dt *Dt_piechart ) Set_radius( r float64 ) {
	if dt != nil {
		dt.Radius = r
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_piechart ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_piechart ) XTo_json( ) ( string ) {
	if dt == nil {
		return "{ }"
	}

	j := fmt.Sprintf( `{ "Kind": %d, "Id": %s, "Xo": %.2f, "Yo": %.2f, "Radius": %.2f, "Filled": %v, "Pcts": [`, 
		dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Radius, dt.Fill_style == sketch.FILL_SOLID )
	sep := ""
	for _, v := range dt.Pcts {
		j += fmt.Sprintf( `%s%.3f`, sep, v )
		sep = ", "
	}

	j += fmt.Sprintf( `], "Fcolours": [ ` )
	sep = ""
	for _, v := range dt.Fcolours {
		j += fmt.Sprintf( `%s%q`, sep, v )
		sep = ", "
	}
	j += fmt.Sprintf( `] }` )

	return j
}

func ( dt *Dt_piechart ) String( ) ( string ) {
	if dt == nil {
		return "<nil>"
	}

	j := fmt.Sprintf( `Kind= %d, Id= %s, Xo= %.2f, Yo= %.2f, Radius= %.2f, Filled= %v, Pcts= [`, 
		dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Radius, dt.Fill_style == sketch.FILL_SOLID )
	sep := ""
	for _, v := range dt.Pcts {
		j += fmt.Sprintf( `%s%.3f`, sep, v )
		sep = ", "
	}

	j += fmt.Sprintf( `] Fcolours= [ ` )
	sep = ""
	for _, v := range dt.Fcolours {
		j += fmt.Sprintf( `%s%s`, sep, v )
		sep = ", "
	}
	j += fmt.Sprintf( `] }` )

	return j
}
