// :vi ts=4 sw=4 noet :
/*
	Mnemonic:	dt_run_graph.go
	Abstract:	The struct and functions needed to implement a run (multiple line) graph.
	Date:		22 May 2018
	Author:		E. Scott Daniels
*/

package drawingthings

import (
	"encoding/json"

	"bitbucket.org/EScottDaniels/sketch"
)


/*
	The struct used to manage the run graph.  Line styles from the array are applied in 
	order.  If less styles are provided than data sets, the styles wrap. If no line styles
	are provided the default is a solid line.
*/
type Dt_run_graph struct {
	Dt_graph_base

	Line_styles	[]int			// line style; one for each line
}

/*
	This generates a run graph struct with defaults. Likely this is used to create
	a graph overwhich a subset of json is used to populate only the desired changes
	to the default.
*/
func Mk_default_run_graph( ) ( *Dt_run_graph ) {

	dt := &Dt_run_graph {
		Dt_graph_base: Dt_graph_base {
			Dt_base: Dt_base {
				Kind: DT_RUNGRAPH,
				Xo:		0.0,			// upper left corner
				Yo:		0.0,
				Visible: true,
			},

			Height:		150.0,
			Width:		250.0,
			Gheight:	100.0,
			Gwidth:		200.0,
			Orig_x:		30.0,				// origin offset in sub page
			Orig_y:		30.0,
			X_disp:		0,
			Y_split:	4,
			X_split:	4,
			Label_size:	8,
			Ymargin:		30,
			Yrmargin:		0,

			Subtitle_size:	10,
			Title_size:		12,
			Title_font:		"Sans",
			Subtitle_font: 	"Sans",
			Title_colour:	"white",
			Subtitle_colour:	"white",

			Data:		nil,
			
			Grid_colour: "#909090", 		// colour for the grid
			Axis_colour:	"white",		// colour for the axis
			Bg_colour:	"#202020",			// colour for the plote area background
			Sp_colour:	"#000000",			// colour for the sub page

			Scale_fmt:	"%.0f",
			Show_xgrid:	true,
			Show_ygrid:	true,
			Show_left_scale: true,		// show the scale on the left axis
			Show_right_scale: false,	// show the scale on the right axis
			Show_xlabels: true,
			Show_left_yaxis: true,		// allow painting of either y axis
			Show_right_yaxis: false,
			
			Right_min:	-1.0,		// use values from data if <0
			Right_max:	-1.0,		// top value for right y axis
			Left_min:	-1.0,		// x-axis value for left y axis
			Left_max:	-1.0,		// top value for left y axis
			Left_round:	-1.0,		// rounding value for top left scale

			Sp_created:	false,		// subpage not yet created
		},
	}

	return dt
}

/*
	Draw_line draws draws a running line connecting each pair in the data.
	Max is the normalisation max value to use.
*/
func ( dt *Dt_run_graph ) draw_lines( gc sketch.Graphics_api, min float64, max float64  ) {

	if dt == nil || dt.Data == nil {
		return
	}

	dg := dt.Data												// convenience
	pspace := (dt.Gwidth - 2) / float64( dg.Get_set_size() )	// spacing of points across x

	ls_idx := 0;
	yo := dt.Orig_y - dt.X_disp

	for set := 0; set < dg.Size(); set++ {			// for each set
		x := dt.Orig_x + 1							// start just a hair off of the axis
		colour := dg.Get_set_colour( set )			// colour for this data
		values := dg.Get_nvalues( set, min, max )	// values normalised 0.0 .. 1.0

		style := 0
		if dt.Line_styles != nil {
			style = dt.Line_styles[ls_idx]
			ls_idx++
			if ls_idx >= len( dt.Line_styles  ) {
				ls_idx = 0
			}
		}
		last_v := float64( 0 )
		for i, v := range values {
			if i > 0 {
				//l := Mk_line( x, dt.Orig_y - (dt.Gheight * last_v),  x + pspace, dt.Orig_y - (dt.Gheight * v ), colour, style )
				l := Mk_line( x, yo - (dt.Gheight * last_v),  x + pspace, yo - (dt.Gheight * v ), colour, style )
				l.Paint( gc, 0.0 )
				x += pspace
			}

			last_v = v;
		}
	}
}

/*
	Paint will render the run graph using the graphics context (gc) passed in.
*/
func ( dt *Dt_run_graph ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil {
		return
	}

	if ! dt.Is_visible( scale ) {
		return
	}

	gc.Push_state()					// mostly to preserve active page
	defer gc.Pop_state()

	if dt.Rot_angle != 0 {
		gc.Translate_delta( dt.Xo + (dt.Width/2), dt.Yo + (dt.Height/2) )		// rotate on the centre of the drawing area
		gc.Rotate( dt.Rot_angle )
		gc.Translate_delta( -(dt.Xo + (dt.Width/2)), -(dt.Yo + (dt.Height/2)) )		// return orign to expected location
	}

	min, max := dt.paint_underlay( gc )				// all things underlying the data (gird, axis, titles)

	dt.draw_lines( gc, float64( min ), float64( max ) )
}


/*
	Return json from the underlying drawing thing.
*/
func ( dt *Dt_run_graph ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}
