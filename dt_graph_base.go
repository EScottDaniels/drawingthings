

/*
	Mnemonic:	dt_graph_base.go
	Abstract:	The base struct for graphs.
	Date:		12 May 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/rouxware/goutils/token"
	"gitlab.com/rouxware/goutils/clike"
	"bitbucket.org/EScottDaniels/sketch"
	"bitbucket.org/EScottDaniels/data_mgr"
)

const (
	XL_DEFAULT	int=iota	// x label styles
	XL_REVERSE
	XL_INDEX				// index in data rather than recorded sequence number
	XL_REV_INDEX			// reverse index order (data in normal order)
	XL_TIME					// hh:mm:ss
	XL_REV_TIME				// hhLmm:ss but max to min
	XL_TIME_HM				// only hh:mm
	XL_REV_TIME_HM			// only hh:mm but max to min
	XL_TIME_H				// only hh
	XL_REV_TIME_H			// only hh but max to min
	XL_DATE
	XL_REV_DATE
	XL_DATE_TIME			// two tiered date and time  (hh:mm)
	XL_REV_DATE_TIME			// two tiered date and time  (hh:mm)
)

/*
	Form base class xo,yo define the lower left of the working area. The coordinates of the 
	origin of the graph are offset from this.  Height and Width are the overall 'clip area,'
	with gheight and gwidth used for the graph itself. 

	It might seem silly to define both the origin offset from the lower left of the 
	graph xo,yo and a Y margin, but the ymargin is used to place the y labels such that
	they are not placed up against the left edge of the drawing area.  Y-margin is the 
	distance to the left of the y axis, not to the right of the drawing area left edge.
*/
type Dt_graph_base struct {
	Dt_base					// common fields and functions

	Height	float64			// overall height,width of the 'sub page'	(before scaling)
	Width	float64
	ScaledH	float64			// height/width after scaling
	ScaledW	float64
	Gheight	float64			// height width of the plot area
	Gwidth	float64
	Orig_x	float64			// the offset of the origin on the sub page
	Orig_y	float64
	X_disp	float64			// displacment (up) from the origin that the xaxis should be written
	Ymargin	int				// y labels on left are written starting this distance to the left of the y axis
	Yrmargin int			// right y title spaced over this many points from the right y axis
	Y_split	int				// number of sections we split in y direction
	X_split	int				// number of splits in x dir
	Label_size	int			// size of x/y label text 
	Subtitle_size int		// size of axis title text
	Title_size int			// size of main title

							// titles are written 5 pts to the inside of the respective subpage edge
	Title		string		// main graph title
	X_title		string		// title under x labels
	Y_ltitle	string		// title on left
	Y_rtitle	string		// title on right
	Title_font	string
	Subtitle_font string

	Data	*data_mgr.Data_group
	
	Grid_colour string		// colour for the grid
	Axis_colour	string		// colour for the axis
	Bg_colour	string		// colour for the plot area background
	Sp_colour	string		// colour for the sub page (background to the whole graph area)
	Title_colour	string	// main title colour
	Subtitle_colour	string	// colur of x/y axis titles
	Label_colour	string	// x and y axis labels
	X_lab_style		int		// style x labels to be written in

	Scale_fmt	string		// format string (e.g. %.0f) used on axis labels
	Show_xgrid	bool
	Show_ygrid	bool
	Show_left_scale bool	// show the scale on the left axis
	Show_right_scale bool	// show the scale on the right axis
	Show_xlabels bool		// show labels along x axis
	Show_left_yaxis bool	// allow painting of either y axis
	Show_right_yaxis bool
	
	Right_min	float64		// x-axis value for right y axis
	Right_max	float64		// top value for right y axis
	Left_min	float64		// x-axis value for left y axis
	Left_max	float64		// top value for left y axis
	Left_round	float64		// top value to round to (e.g. 100 or 1000)
	
	Sp_created	bool		// indicates whether or not the subpage has been created
	IsScaled	bool		// true if scaled height/width should be used
	scaleReset	bool		// set to true if the scale was changed after last paint

	preserved_data	*data_mgr.Data_group
	supports_rotation	bool
}

// === internal functions ===============================================================

/*
	Convert a unix timestamp into month, day, year value
*/
func ts2mdy( ts int64 ) ( m int, d int, y int ) {
	t := time.Unix( ts, 0 )

	return int( t.Month() ), t.Day(), t.Year()
}

// ---- axis and grid painting -----------------------------------------
/*
	Givn a uxix timestamp  return the hour, minute second.
*/
func ts2hms( ts int64 ) ( h int, m int, s int ) {
	t := time.Unix( ts, 0 )

	return t.Hour(), t.Minute(), t.Second()
}

func  ( dt *Dt_graph_base ) paint_axis( gc sketch.Graphics_api ) {
	if dt == nil {
		return
	}

	gc.Set_colour( dt.Axis_colour )
	gc.Draw_line( dt.Orig_x, dt.Orig_y-dt.X_disp, dt.Orig_x + dt.Gwidth, dt.Orig_y-dt.X_disp  )		// x axis
	gc.Draw_line( dt.Orig_x, dt.Orig_y, dt.Orig_x, dt.Orig_y - dt.Gheight )		// y axis
}

/*
	Paint just the x-axis.
*/
func  ( dt *Dt_graph_base ) paint_xaxis( gc sketch.Graphics_api ) {
	if dt == nil {
		return
	}

	gc.Set_colour( dt.Axis_colour )
	gc.Draw_line( dt.Orig_x, dt.Orig_y-dt.X_disp, dt.Orig_x + dt.Gwidth, dt.Orig_y-dt.X_disp  )		// x axis
}

/*
	Paint_ygrid draws the grid in the y-direction (horizontal lines, top to bottom )using the grid colour.
*/
func  ( dt *Dt_graph_base ) paint_ygrid( gc sketch.Graphics_api ) {
	if dt == nil {
		return
	}

	step :=  dt.Gheight  / float64( dt.Y_split )			// distance between lines

	gc.Push_state()
	gc.Set_line_width( 0 )									// thinest supported
	gc.Set_colour( dt.Grid_colour )

	y := dt.Gheight
	for i := 0; i <= dt.Y_split; i++ {
		gc.Draw_line( dt.Orig_x - 5, dt.Orig_y - y, dt.Orig_x + dt.Gwidth, dt.Orig_y - y  )		// parallel to the x axis
		y -= step
	}

	gc.Pop_state()
}

/*
	Paint_ygrid draws the grid in the x-direction (vertical lines, left to right )using the grid colour.
*/
func  ( dt *Dt_graph_base ) paint_xgrid( gc sketch.Graphics_api ) {
	if dt == nil {
		return
	}

	step := dt.Gwidth / float64( dt.X_split )			// distance between lines

	gc.Push_state()
	gc.Set_line_width( 0 )									// thinest supported
	gc.Set_colour( dt.Grid_colour )

	x := step
	for i := 0; i < dt.X_split; i++ {
		gc.Draw_line( dt.Orig_x + x, dt.Orig_y + 5, dt.Orig_x + x, dt.Orig_y - dt.Gheight )		// parallel to the y axis
		x += step
	}

	gc.Pop_state()
}

/*
	Paint_y_labels writes the values for each of the y-axis grid lines.
*/
func ( dt *Dt_graph_base ) paint_y_labels( gc sketch.Graphics_api, min float64, max float64, fmtstr string, on_right bool ) {
	if dt == nil  {
		return
	}

	if fmtstr == "" {
		fmtstr = "%.0f"
	}

	margin := float64( 0 ) 
	if on_right {
		margin = dt.Orig_x + dt.Gwidth + 5.0
	} else {
		margin = dt.Orig_x - float64( dt.Ymargin )
	}

	vstep := (max - min) / float64( dt.Y_split )			// label value step
	step :=  dt.Gheight  / float64( dt.Y_split )			// distance between labels
	y := dt.Gheight

	gc.Set_font( "Courier", dt.Label_size, 1, false )
	gc.Set_colour( dt.Label_colour )
	for i := 0; i <= dt.Y_split; i++ {						// want to include value on axis too
		str := fmt.Sprintf( fmtstr, max )
		gc.Draw_text(  margin, dt.Orig_y - y, str )
		max -= vstep
		y -= step
	}
}

/*
	paint_x_labels draws labels along the x-axis.  Style is one of our x-label constants (DL_*) from above.

	For the index and reverse index styles, the max and min parms are ignored and we assume we start at
	0 (min) and go to max.

	for time formats, max and min are assumed to be UNIX timestamps and format string is ignored.
*/
func ( dt *Dt_graph_base ) paint_x_labels( gc sketch.Graphics_api, min float64, max float64, fmtstr string ) {
	if dt == nil || dt.X_split <= 0 {
		return
	}

	gc.Set_font( "Courier", dt.Label_size, 1, false )
	gc.Set_colour( dt.Label_colour )

	x := dt.Orig_x
	y := dt.Orig_y + float64(dt.Label_size + 5)
	step :=  dt.Gwidth  / float64( dt.X_split )			// distance between labels
	vstep := (max - min) / float64( dt.X_split )			// label value step

	switch dt.X_lab_style {
		case XL_REV_INDEX:									// simple reverse numeric progression using index of the data
			min = 0
			max = float64( dt.Data.Get_set_size() )
			vstep = float64( dt.Data.Get_set_size()  / dt.X_split )
			fallthrough

		case XL_REVERSE:			// reverse numeric progression
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				str := fmt.Sprintf( fmtstr, max )
				gc.Draw_text(  x, y, str )
				max -= vstep
				x += step
			}

		case XL_INDEX:									// simple numeric progression using index of the data rather than the sequence value
			min = 0
			vstep = float64( dt.Data.Get_set_size()  / dt.X_split )
			fallthrough

		case XL_DEFAULT:								// simple numeric progression using values in the data
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				str := fmt.Sprintf( fmtstr, min )
				gc.Draw_text(  x, y, str )
				min += vstep
				x += step
			}


		case XL_TIME:
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, m, s := ts2hms( int64( min ) )
				str := fmt.Sprintf( "%02d:%02d:%02d", h, m, s )
				gc.Draw_text(  x, y, str )
				min += vstep
				x += step
			}

		case XL_REV_TIME:											// time but from max to min
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, m, s := ts2hms( int64(  max ) )
				str := fmt.Sprintf( "%02d:%02d:%02d", h, m, s )
				gc.Draw_text(  x, y, str )
				max -= vstep
				x += step
			}

		case XL_TIME_HM:											// time hour and minute only
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, m, _ := ts2hms( int64( min ) )
				str := fmt.Sprintf( "%02d:%02d", h, m )
				gc.Draw_text(  x, y, str )
				min += vstep
				x += step
			}

		case XL_REV_TIME_HM:											// time but from max to min
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, m, _ := ts2hms( int64( max ) )
				str := fmt.Sprintf( "%02d:%02d", h, m )
				gc.Draw_text(  x, y, str )
				max -= vstep
				x += step
			}

		case XL_TIME_H:												// time hour only
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, _, _ := ts2hms( int64( min ) )
				str := fmt.Sprintf( "%02d", h )
				gc.Draw_text(  x, y, str )
				min += vstep
				x += step
			}

		case XL_REV_TIME_H:											// time but from max to min, hour only
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				h, _, _ := ts2hms( int64( max ) )
				str := fmt.Sprintf( "%02d", h )
				gc.Draw_text(  x, y, str )
				max -= vstep
				x += step
			}

		case XL_DATE:
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				m, d, year := ts2mdy( int64( min ) )
				str := fmt.Sprintf( "%d/%02d/%02d", year, m, d )
				gc.Draw_text(  x, y, str )
				min += vstep
				x += step
			}

		case XL_REV_DATE:
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				m, d, year := ts2mdy( int64( max ) )
				str := fmt.Sprintf( "%d/%02d/%02d", year, m, d )
				gc.Draw_text(  x, y, str )
				max -= vstep
				x += step
			}

		case XL_DATE_TIME:											// two tier date and time string
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				m, d, year := ts2mdy( int64( min ) )
				hr, minute, _ := ts2hms( int64( min ) )

				str := fmt.Sprintf( "%d/%02d/%02d", year, m, d )
				gc.Draw_text(  x, y, str )

				str = fmt.Sprintf( "%02d:%02d", hr, minute )
				gc.Draw_text(  x, y - float64(dt.Label_size + 2), str )
				min += vstep
				x += step
			}

		case XL_REV_DATE_TIME:											// two tier date and time string
			for i := 0; i <= dt.X_split; i++ {						// want to include value on axis too
				m, d, year := ts2mdy( int64( max ) )
				hr, minute, _ := ts2hms( int64( max ) )

				str := fmt.Sprintf( "%d/%02d/%02d", year, m, d )
				gc.Draw_text(  x, y, str )

				str = fmt.Sprintf( "%02d:%02d", hr, minute )
				gc.Draw_text(  x, y - float64(dt.Label_size + 2), str )
				max -= vstep
				x += step
			}
	}
}


/*
	Paint the various titles if they aren't nil
*/
func ( dt *Dt_graph_base ) paint_titles( gc sketch.Graphics_api ) {
	if dt == nil {
		return
	}

	if dt.Title != "" {
		gc.Set_colour( dt.Title_colour )
		gc.Set_font( dt.Title_font, dt.Title_size, 1, false )
		gc.Draw_text( dt.Orig_x,  5 + float64( dt.Title_size ), dt.Title )
	}

	need_font := true

	if ! dt.supports_rotation {
		return
	}

	if dt.Y_ltitle != "" {
		need_font = false
		gc.Set_font( dt.Subtitle_font, dt.Subtitle_size, 1, false )
		gc.Set_colour( dt.Subtitle_colour )

		gc.Push_state( )
		gc.Translate_delta( 0, dt.Orig_y )
		if gc.Rotate( 90 ) {
			gc.Draw_text(  0, -(5 + float64(dt.Subtitle_size) ), dt.Y_ltitle )
		} else {
			dt.supports_rotation = false
		}
		gc.Pop_state( )
	}

	if dt.Y_rtitle != "" {
		if need_font  {
			gc.Set_font( dt.Subtitle_font, dt.Subtitle_size, 1, false )
			gc.Set_colour( dt.Subtitle_colour )
		}

		gc.Push_state( )
		gc.Translate_delta( float64( dt.Yrmargin ) + dt.Orig_x + dt.Gwidth + 5, dt.Orig_y )
		if gc.Rotate( 90 ) {
			gc.Draw_text(  0, -(5 + float64(dt.Subtitle_size) ), dt.Y_rtitle )
		} else {
			dt.supports_rotation = false
		}
		gc.Pop_state( )
	}
}

// ------------------------------------------------------------------

/*
	Preserve allows the drawingthing to stash away information that it wants to 
	ensure does not change during an update.  By default we save/restore just the 
	ID and kind.  Each object may decide to do more and implement their own
	version of this function.
*/
func (dt *Dt_graph_base) Preserve() {
	if dt == nil {
		return
	}

	dt.preserved_id = dt.Id
	dt.preserved_kind = dt.Kind
	dt.preserved_data = dt.Data
}

/*
	Restore allows the drawing thing to restore anything preserved, and/or to force
	additional computations on the next paint assuming that something affecting
	the paint (size, location, etc.) has changed.  As a default, the ID will be
	restored and the updated boolean will be marked true.
*/
func (dt *Dt_graph_base) Restore() {
	if dt == nil {
		return
	}

	dt.Id = dt.preserved_id
	dt.Kind = dt.preserved_kind
	dt.Data = dt.preserved_data
	dt.updated = true
}

/*
	Set data  does one of two things depending on what is passed in:

	If the data_thing is a data_manger group or set, it 
	attaches a data set to the graph. The data set is used
	when the paint function is called, but otherwise is assumed to 
	be managed outside of the drawing thing environment.

	When the data_thing is a string, it is assumed to conain information to 
	apply to the already attached data group. The string is assumed to 
	have information of the form:

		<set>:<seq>,<value> [<set>:<seq,<value>]  ...

	The sequence value allows for both x,y plotting as well as either sequence series 
	run/bar graphis or time oriented where seq is a unix timestamp. In any case
	it is treated as a float64.
*/
func ( dt *Dt_graph_base ) Set_data( data_thing interface{} ) {
	var (
		dstr string			// a data update stiring	
	)

	if dt == nil { 
		return
	}

	switch data :=  data_thing.( type ) {
			case *data_mgr.Data_group:
					dt.Data = data
					return

			case *data_mgr.Data_set:
					colours := []string{ data.Get_colour() }
					dt.Data = data_mgr.Mk_data_group( 1, data.Size(), colours )
					return

			case *string:
				dstr = *data

			case string:
				dstr = data

			default:
				return			// bail on unknown
	}
	
	if dt.Data == nil {
		return
	}

	ntoks, tokens := token.Tokenise_qsep( dstr, " \t" )          // allow space or tabs to separate

	for i := 0; i < ntoks; i++ {
		if tokens[i] == "" {
			continue 
		}

		svtoks := strings.Split( tokens[i], ":" )
		if len( svtoks ) == 2 {
			set := clike.Atoi( svtoks[0] )
			svtoks = strings.Split( svtoks[1], "," )
			if len( svtoks ) == 2 {
				seq := clike.Atof( svtoks[0] )
				val := clike.Atof( svtoks[1] )

				//fmt.Printf( ">>> adding set: %d, %.2f, %.2f\n", set, seq, val )
				dt.Data.Add_set_sv( set, seq, val )
			}
		}
	}
}

/*
	Set the desired scale. This applies only to the overall height/width
	used to manage the subpage. All other aspects of scaling the actual
	paint process are handled in the GC.
*/
func ( dt *Dt_graph_base ) Set_scale( scale float64 ) {
	if dt != nil {
		dt.ScaledH = dt.Height * scale
		dt.ScaledW = dt.Width * scale
		dt.IsScaled = true
		dt.scaleReset = true	// cause recreation of subpage on paint
	}
}

/*
	Paints the underlying things (grid). Returns the computed min and max 
	value.  We always push the min value to 0, but it will go negative
	if it needs to based on the adjustment of the x axis and values in data.
*/
func ( dt *Dt_graph_base ) paint_underlay( gc sketch.Graphics_api ) ( min float64, max float64 ) {
	if dt == nil {
		return 0, 0
	}

	if dt.scaleReset {	// scale was adjusted; ensure subpage is recreated with new scale values
		if  dt.Sp_created {
			gc.Delete_subpage( dt.Id )
			dt.Sp_created = false
		}

		dt.scaleReset = false
	}

	if ! dt.Sp_created {
		h := dt.Height
		w := dt.Width
		if dt.IsScaled {
			h = dt.ScaledH
			w = dt.ScaledW
		}
		gc.Mk_subpage( dt.Id, dt.Xo, dt.Yo, h, w )		// create the subpage to draw in  (x,y is upper left)
		dt.Sp_created = true
		dt.supports_rotation = true
	}

	gc.Select_page( dt.Id )
	gc.Set_colour( "green" )
	gc.Clear_subpage( dt.Sp_colour )			// allows page colour to change dynamically

	min, max = dt.Data.Get_value_range( )		// for scale
	if min > 0 {
		min = 0
	}
	if max < dt.Left_max {
		max = dt.Left_max
	}
	if dt.Left_round > 0 {
		max =  (max + dt.Left_round) -  float64( int( dt.Left_round )  % 1000 )
	}

	smin, smax := dt.Data.Get_seq_range( )							// sequence min/max for labels on x-axis

	gc.Set_fill_attrs( dt.Sp_colour, sketch.FILL_SOLID )													// fill the graph background
	r := Mk_rect( dt.Orig_x, dt.Orig_y - dt.Gheight, dt.Gheight, dt.Gwidth, "none", true, dt.Bg_colour )	// graph proper background
	r.Paint( gc, 0.0 )

	if dt.Show_ygrid {
		dt.paint_ygrid( gc )
	}

	if dt.Show_xgrid {
		dt.paint_xgrid( gc )
	}

	if dt.Show_left_yaxis {
		dt.paint_axis( gc )		// paint both
	} else {
		dt.paint_xaxis( gc )	// paint only the x axis
	}

	if dt.Show_left_scale {
		nppx := max / ( dt.Gheight - dt.X_disp )				// number value per px in pos
		if dt.X_disp > 0 {
			neg := math.Abs( min / ( dt.X_disp ) )					// number value per px in neg
			if neg > nppx {
				nppx = neg
			}
		}
		min = -(nppx * dt.X_disp)
		max = nppx * (dt.Gheight - dt.X_disp)
		dt.paint_y_labels( gc, min, float64( max ), dt.Scale_fmt, false )		// labels on left
	}

	if dt.Show_right_scale {
		dt.paint_y_labels( gc, 0, float64( max ), "%.0f", true )	// labels on right
	}

	if dt.Show_xlabels {
		dt.paint_x_labels( gc, smin, smax, dt.Scale_fmt )
	}

	dt.paint_titles( gc )

	return float64( min ), float64( max )
}

/*
	Nuke will clean up the subpage associated with the drawing thing; if it exists.
	State is saved such that the active page remains the same.  The drawing thing
	is assumed to be on the curently selected page, or on the root page.
*/
func ( dtg *Dt_graph_base ) Nuke( gc sketch.Graphics_api ) {
	if dtg == nil {
		return
	}

	gc.Push_state()
	gc.Delete_subpage( dtg.Id )		// searches the currently active subpage, then root
	gc.Pop_state()
}

// ------ public functions ------------------------------------------
/*
	Create a base graph with defaults.  
	This graph probalby has no value other than a place holder as it has no
	real paint function associated with it.
*/
func Mk_base_graph( xo float64, yo float64, height float64, width float64, gh float64, gw float64 ) ( *Dt_graph_base ) {
	dt := &Dt_graph_base{
		Dt_base:  Dt_base {
			Id: "base_graph",
			Xo: xo,
			Yo: yo,
			Kind:	DT_GRAPH,
			Visible: true,
		},

		Axis_colour: "white",
		Grid_colour: "#808080",
		Bg_colour: "#202020",	
		Sp_colour: "#000000",	
		Height:		height,
		Width:		width,
		Gheight:	gh,
		Gwidth:		gw,	
		X_disp:		0,
		X_split:	4,
		Y_split:	4,
		Orig_x:		30,
		Orig_y:		height - 20,
		Label_size: 8,
		Scale_fmt:	"%.0f",
		Subtitle_size: 12,
		Label_colour: "#a0a0a0",
		Show_xlabels: true,
		Show_left_yaxis: true, 
		Show_left_scale: true,
		Show_xgrid: true,
		Show_ygrid: true,
		Title_size: 14,
		Ymargin:	30,

		Sp_created: false,
		IsScaled:	false,
	}

	return dt
}

/*
	Paint will paint the base graph.  Probably useless as there is no data style associated
	with the base graph and thus nothing is plotted. 
*/
func ( dt *Dt_graph_base ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil {
		return
	}

	if ! dt.Is_visible( scale ) {
		return
	}

	dt.paint_underlay( gc )				// just paint the underlying things
}

