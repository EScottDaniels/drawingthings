

/*
	Testing for the various shape things

	Run by itself with:
		CGO_ENABLED=1 go test -run image

	NOTE:  This test requires that image files be present in the /tmp directory.
		images are NOT checked into the source repo (binary files do NOT belong 
		there!).  Any gif, jpeg, or png should do.  
		(Future: add script to pull NWS radar images for testing)
*/

package drawingthings_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"bitbucket.org/EScottDaniels/drawingthings"
	"bitbucket.org/EScottDaniels/sketch"
)

var (
	common_gc sketch.Graphics_api = nil		// a single graphics context for all tests in this module
)

/*
	Generate a graphics context for either postscript or xi interface.
*/
func get_gc( ) ( sketch.Graphics_api, error ) {
	var (
		err error = nil
	)

	if common_gc == nil {
		common_gc, err = sketch.Mk_graphics_api( "xi", ",900,1100,Test" )
	}

	if err == nil {
		common_gc.Set_scale( 2, 2 )
	}

	return common_gc, err
}

func Test_image( t *testing.T )  {
	var (
		err error
	)
	
	fmt.Fprintf( os.Stderr, "[INFO] image test starts\n" )

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	jstr := fmt.Sprintf( `{
			"Kind": %d,
			"Id": "test_sketch_image_plain",
			"Colour": "#9090f0",
			"Xo":	10,
			"Yo":	30,
			"Rot": 0,
			"Height":  300,
			"Width": 300,
			"Fname": "/tmp/image_test.jpg",
			"Border": true,
			"Rfreq": 900,
			"Visible": true }`, 
		drawingthings.DT_IMAGE )

	si_jstr := fmt.Sprintf( `{
			"Kind": %d,
			"Id": "test_sketch_image_stacked",
			"Colour": "#90f090",
			"Xo":	350,
			"Yo":	30,
			"Rot": 0,
			"Height":  300,
			"Width": 300,
			"Fname": "/tmp/simage1_test.gif,/tmp/simage2_test.gif,/tmp/simage3_test.gif",
			"Border": true,
			"Rfreq": 900,
			"Visible": true }`, 
		drawingthings.DT_IMAGE )

	m_jstr := fmt.Sprintf( `{
			"Kind": %d,
			"Id": "test_sketch_image_stacked",
			"Colour": "#f09090",
			"Xo":	10,
			"Yo":	350,
			"Rot": 0,
			"Height":  300,
			"Width": 300,
			"Fname": "/tmp/simage1_test.gif;/tmp/simage2_test.gif;/tmp/simage3_test.gif",
			"Border": true,
			"Rfreq": 900,
			"Visible": true }`, 
		drawingthings.DT_IMAGE )

	j := []string { jstr, si_jstr, m_jstr }
	dts := make( []drawingthings.Drawingthing_i, len( j ) )
	for i := 0; i < len( j ); i++ {
		dts[i], err = drawingthings.Json2ScaledDt( j[i], 1.0 )

		if err != nil {
			fmt.Fprintf( os.Stderr, "[FAIL] image test: unable to create dt from json: %s\n%s\n", j[i], err ) 
			t.Fail()
			return
		} else {
			fmt.Fprintf( os.Stderr, "[OK]   image test: created image\n" );
		}
	
		if dts[i] == nil {
			fmt.Fprintf( os.Stderr, "[FAIL] image test: returned pointer was nil for index=%d!\n", i )
			t.Fail()
			return
		}

		dts[i].Set_scale( 2.0 )		// images must be scaled before their first paint so they load at scale
		dts[i].Paint( gc, 0.0 )		// causes reload, but won't actually paint 
	}

	time.Sleep( 2 * time.Second )			// pause to allow loads to complete
	for i := 0; i < len( dts ); i++ {
		dts[i].Paint( gc, 1.0 )				// now paint what was loaded above
	}

	gc.Show( )	
	gc.Clear()								// prep to paint into double buffer

	fmt.Fprintf( os.Stderr, "[INFO] resetting scale to 1:1,  then reloading" )
	gc.Set_scale( 1.0, 1.0 )						// set the scale, force a reload, then redisplay 
	for i := 0; i < len( dts ); i++ {
		dts[i].Set_scale( 1.0 )		// images must be scaled before their first paint so they load at scale
		dts[i].Paint( gc, 1.0 )				// now paint what was loaded above
	}

	time.Sleep( 10 * time.Second )			// delay while the large images are viewed by human
	for i := 0; i < len( dts ); i++ {
		dts[i].Paint( gc, 1.0 )				// now paint what was loaded above
	}
	gc.Show( )	
	time.Sleep( 10 * time.Second )			// delay while the large images are viewed by human

	fmt.Fprintf( os.Stderr, "[INFO] image test finished\n" )
}

