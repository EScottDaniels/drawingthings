
/*
	Testing of ability to make use of the sketch graphics paint interface.
*/

package drawingthings

import (
	"fmt"
	"os"
	"math/rand"
	"time"
	"testing"

	"bitbucket.org/EScottDaniels/data_mgr"
	"bitbucket.org/EScottDaniels/sketch"
)

var (
	common_gc sketch.Graphics_api = nil
	gen_ps bool = false
)

func draw_grid( gc sketch.Graphics_api, colour string ) {
	y := 50
	for i := 0; i < 20; i++ {
		jstr := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Id": "test_sketch_line", 
			"Xo":	0, 
			"Yo":	%d, 
			"X1":  1600, 
			"Y1": %d, 
			"Colour": %q,
			"Fcolour": "#404040",
			"Visible": true }`, DT_LINE, y, y, colour )

		dt, _ := Json2ScaledDt( jstr, 1.0 )
		y += 50
		dt.Paint( gc, 1.0 )
	}

	x := 50
	for i := 0; i < 20; i++ {
		jstr := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Id": "test_sketch_line", 
			"Xo":	%d, 
			"Yo":	0, 
			"X1":  %d, 
			"Y1": 800, 
			"Colour": %q,
			"Fcolour": "#404040",
			"Visible": true }`, DT_LINE, x, x, colour )

		dt, _ := Json2ScaledDt( jstr, 1.0 )
		x += 50
		dt.Paint( gc, 1.0 )
	}
}

/*
	Generate json, and write to stderr.
*/
func gen_json( dt Drawingthing_i  ) ( error ) {
	jstr, err := dt.To_json( )
	if err == nil {
		fmt.Fprintf( os.Stderr, "[EXAM} %s\n", jstr )
	}

	return err
}

/*
	Generate a graphics context for either postscript or xi interface.
*/
func get_gc( ) ( sketch.Graphics_api, error ) {
	var (
		err error = nil
	)

	if common_gc == nil {
		if gen_ps {
			common_gc, err = sketch.Mk_graphics_api( "postscript", "sketch.ps" )
		} else {
			common_gc, err = sketch.Mk_graphics_api( "xi", ",900,1100,Test" )
			common_gc.Translate( 0, 0 )
		}
	}

	return common_gc, err
}

/*
	Generate a text drawing thing.
*/
func mk_txt_dt( x int, y int, colour string, size int, font string, text string ) ( Drawingthing_i, error ) {
	jstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "unidentifed_text",
			"Colour": %q,
			"Text": %q,
			"Xo":	%d,
			"Yo":	%d,
			"Rot": 0,
			"Size": %d,
			"Font": %q,
			"Visible": true }`, 
		DT_TEXT, colour, text, x, y, size, font )

	dt, err := Json2ScaledDt( jstr, 1.0 )	
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] could not alloc text string thing: %s", err )
		return nil, err
	}

	return dt, nil
}

// ---------------------- actual tests --------------------------------

/*
	Clock and text test
*/
func Test_sketch_text( t *testing.T )  {
	jstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "test_sketch_test",
			"Colour": "#f0f000",
			"Text": "@30,500: Now is the time for all good programmers to write in real languages (green and yellow)",
			"Xo":	30,
			"Yo":	500,
			"Rot": 0,
			"Size":  8,
			"Font": "Sans",
			"Visible": true }`, 
		DT_TEXT )

	cjstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "clock",
			"Colour": "white",
			"Xo":	30,
			"Yo":	130,
			"Format": "2 Jan 2006 15:04:05",
			"Size": 24,
			"Font": "Sans",
			"Visible": true }`, 
		DT_CLOCK )

	dt, err := Json2ScaledDt( jstr, 1.0 )		// convert json to  drawing things
	cdt, cerr := Json2ScaledDt( cjstr, 1.0 )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create text dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if cerr != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create clock dt from base json: %s\n",cerr ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to text dt was nil!\n" )
		t.Fail()
		return
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	draw_grid( gc, "#707070" )
	cdt.Paint( gc, 1.0 )
	dt.Paint( gc, 1.0 )							// intial paint

	dt.Set_colour( "green" )
	dt.Set_rel_origin( 10, 30 )				// move and paint another, in green with some rotation
	dt.Rotate( 0, 32 )						// rotated 45 degrees around the center point
	dt.Paint( gc, 1.0 )

	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}

	fmt.Fprintf( os.Stderr, "[INFO] text test finished\n" )
}

func Test_sketch_rect( t *testing.T )  {
	jstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "test_sketch_rect",
			"Colour": "#909090",
			"Xo":	100,
			"Yo":	300,
			"Rot": 0,
			"Height":  100,
			"Width": 100,
			"Fill_style": %d,
			"Fcolour": "#900090",
			"Visible": true }`, 
		DT_RECT, sketch.FILL_SOLID )
	jstr_rotated := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "test_sketch_rect",
			"Colour": "white",
			"Fcolour": "blue",
			"Xo":	100,
			"Yo":	300,
			"Rot": 0,
			"Height":  100,
			"Width": 100,
			"Fill_style": %d,
			"Rot_angle": 10,
			"Rot_vertex": 0,
			"Visible": true }`, 
		DT_RECT, sketch.FILL_NONE )

	dt, err := Json2ScaledDt( jstr, 1.0 )		// convert json to a drawing thing

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to rect dt was nil!\n" )
		t.Fail()
		return
	}

	dtr, _ := Json2ScaledDt( jstr_rotated, 1.0 )

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Rectangle Test" )
	title.Paint( gc, 1.0 )

	dt.Paint( gc, 1.0 )

	dt.Set_rel_origin( 0, 200 )				// move and paint another, in green
	dt.Rotate( 0, 45 )						// rotated 45 degrees around the center point
	dt.Paint( gc, 1.0 )

	dtr.Paint( gc, 1.0 )
	dtr.Rotate( 1, 20 )
	dtr.Paint( gc, 1.0 )
	dtr.Rotate( 3, -20 )
	dtr.Paint( gc, 1.0 )
	dtr.Rotate( 4, 50 )
	dtr.Paint( gc, 1.0 )

	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}

	fmt.Fprintf( os.Stderr, "[INFO] rect test finished\n" )
}

func Test_sketch_reg_poly( t *testing.T ) {
	one_reg_poly( t, 3 )
	one_reg_poly( t, 4 )
	one_reg_poly( t, 5 )
	one_reg_poly( t, 6 )
	one_reg_poly( t, 10 )
}

func one_reg_poly( t *testing.T, nsides int )  {
	jstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Fill_style": %d,
			"Sides": %d,

			"Id": "test_sketch_poly",
			"Colour": "#ffffff",
			"Xo":	100,
			"Yo":	200,
			"Rot": 0,
			"Radius": 50,
			"Scale": 1,
			"Fcolour": "#004090",
			"Rot_angle": 0,
			"Rot_vertex": 0,
			"Visible": true
			}`, 
		DT_REGPOLY, sketch.FILL_SOLID, nsides )

	fmt.Fprintf( os.Stderr, "\n[INFO] regular polygon test starts\n" )
	dt, err := Json2ScaledDt( jstr, 1.0 )		// convert json to a drawing thing

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create regular polygon dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to regular poly dt was nil!\n" )
		t.Fail()
		return
	}

	jstr, err = dt.To_json()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] unable to generate json from polygon: %s\n", err )
		t.Fail()
	} else {
		fmt.Fprintf( os.Stderr, "[EXAM] %s\n", jstr )
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	draw_grid( gc, "#303030" )
	title, _ := mk_txt_dt( 200, 20, "white", 14, "Sans", fmt.Sprintf( "Regular Poly Test: %d sides", nsides ) )
	title.Paint( gc, 1.0 )

	fmt.Fprintf( os.Stderr, "[INFO] regular poly painting \n" )
	dt.Paint( gc, 1.0 )

	dt.Rotate( 0, 45.0 )
	dt.Set_origin( 200, 200 )
	dt.Set_colour( "red" )
	dt.Set_line_style( DOT_DOT_DASH_LINE )
	dt.Set_fill( false )
	dt.Paint( gc, 1.0 )

	dt.Rotate( 0, 90.0 )
	dt.Set_colour( "green" )
	dt.Set_origin( 300, 200 )
	dt.Set_line_style( DOT_LINE )
	dt.Set_fill( false )
	dt.Paint( gc, 1.0 )

	dt.Rotate( 0, 270.0 )
	dt.Set_origin( 400, 200 )
	dt.Set_colour( "orange" )
	dt.Set_fill( false )
	dt.Set_line_style( DASHED_LINE )
	dt.Set_scale( 0.5 )
	dt.Paint( gc, 1.0 )

	dt.Set_line_style( SOLID_LINE )
	gc.Show()

	if ! gen_ps {
		time.Sleep( 10 * time.Second )
	}

	fmt.Fprintf( os.Stderr, "[INFO] regular poly test finished\n" )
}

func Test_sketch_tangle( t *testing.T )  {
	jstr := fmt.Sprintf( `{ 
			"Kind": %d,
			"Id": "test_sketch_tangle",
			"Colour": "white",
			"Xo":	400,
			"Yo":	800,
			"Rot": 0,
			"Scale": 1,

			"Height":  300,
			"Base_len": 50,
			"Peak_offset": 50,
			"Rise": 80,

			"Fill_style": %d,
			"Fcolour": "#300030",
			"Rot_angle": 0,
			"Rot_vertex": 0,
			"Visible": true,
			"Align_centre": false }`, 
		DT_TANGLE, sketch.FILL_SOLID )

	jstr_rotated := fmt.Sprintf( `{ 
			"Kind": %d,
			"Fill_style": %d,
			"Line_style": %d, 

			"Id": "test_sketch_tangle",
			"Colour": "white",
			"Xo":	450,
			"Yo":	720,
			"Rot": 0,

			"Height":  220,
			"Base_len": 50,
			"Rise": -80,
			"Peak_offset": 0,

			"Fcolour": "#005050",
			"Rot_angle": 0,
			"Rot_vertex": 0,
			"Visible": true,
			"Align_centre": false }`, 
		DT_TANGLE, sketch.FILL_NONE, DASHED_LINE )

	//jstr_update := `{ "Colour": "#4ff0000", "Line_style": 0 }`

	fmt.Fprintf( os.Stderr, "\n[INFO] triangle test starts\n" )
	dt, err := Json2ScaledDt( jstr, 1.0 )		// convert json to a drawing thing

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create triangle dt from base json: %s\n",err ) 
		t.Fail()
		return
	}
 
	dtr, err := Json2ScaledDt( jstr_rotated, 1.0 )		// convert json to a drawing thing

	if dt == nil || dtr == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to triangle dt was nil!\n" )
		t.Fail()
		return
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}


	fmt.Fprintf( os.Stderr, "[INFO] triangle painting \n" )
	//dt.Paint( gc, 1.0 )

	a := 0
	b := 0
	for i := 0; i < 720; i++ {
		title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Triangle Test" )
		title.Paint( gc, 1.0 )

		b += 1
		a -= 1
		if b >= 360 {
			b = b - 360 
		} else {
			if a < 0 {
				a= 359
			}
		}

		draw_grid( gc, "#005020" )
		dt.Paint( gc, 1.0 )
		dtr.Paint( gc, 1.0 )
		gc.Show()
		dt.Rotate( 3, float64( a ) )		// 0 is rotate round the center
		dtr.Rotate( 3, float64( a ) )		// 0 is rotate round the center
		
		if b == 1 {
			time.Sleep( 2 * time.Second )
		} else {
			time.Sleep( 25 * time.Millisecond )
		}
	}

	dtr.Paint( gc, 1.0 )

	dt.Set_origin( 200, 200 )
	dt.Set_scale( 1.0 )
	dt.Set_colour( "green" )
	dt.Rotate( 1, 90.0 )
	dt.Paint( gc, 1.0 )

	dt.Set_origin( 3, 300 )
	dt.Set_scale( 1.0 )
	dt.Set_colour( "orange" )
	dt.Rotate( 2, 40.0 )
	dt.Paint( gc, 1.0 )

	//Json_update( dtr, jstr_update )
	dt.Set_origin( 300, 200 )
	dt.Set_colour( "white" )
	dt.Rotate( 3, 60.0 )
	dt.Paint( gc, 1.0 )

	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 15 * time.Second )
	}

	fmt.Fprintf( os.Stderr, "[INFO] triangle test finished\n" )
}

/*
	Test line.  The end point of the line should be in the middle of the triangle as
	triangle is drawn centered on x,y of 400,100.
*/
func Test_sketch_line( t *testing.T )  {
	jstr := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Id": "test_sketch_line", 
			"Colour": "#a000ff", 
			"Xo":	400, 
			"Yo":	300, 
			"X1":  400, 
			"Y1": 100, 
			"Fcolour": "#000001", 
			"Visible": true }`, DT_LINE )

	dt, err := Json2ScaledDt( jstr )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to line was nil!\n" )
		t.Fail()
		return
	}

	dt.Set_endpt( 103.7, 94.5 )
	fmt.Fprintf( os.Stderr, "[OK]   created dt from base json: %s\n", dt ) 

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Lines Test" )
	title.Paint( gc, 1.0 )

	dt.Set_line_style( DOT_DOT_DASH_LINE )
	dt.Paint( gc, 1.0 )
	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}

	fmt.Fprintf( os.Stderr, "[INFO] sketch: line test finished\n" )
}


func Test_sketch_pie( t *testing.T )  {

	values := []float64{ 50, 100, 300, 900,150 } 
	colours := []string { "red", "orange", "green", "blue", "pink"  }
	dt := Mk_piechart_values( 300, 100, 80, "#9000f0", "white", values, colours )

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to piechart thing was nil!\n" )
		t.Fail()
		return
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Pie Test" )
	title.Paint( gc, 1.0 )

	dt.Paint( gc, 1.0 )
	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}

	gen_json( dt )

	fmt.Fprintf( os.Stderr, "[INFO] sketch: pie chart sketch  test finished\n" )
}


/*
	Test the basic graph
*/
func Test_sketch_graph( t *testing.T )  {

	dt := Mk_base_graph( 200, 200, 200, 400, 100, 300 )

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to graph thing was nil!\n" )
		t.Fail()
		return
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	gc.Select_page( "root" )
	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Generic Graph  Test" )
	title.Paint( gc, 1.0 )

	dt.Set_data( nil )
	dt.Paint( gc, 1.0 )

	gc.Show( )	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}

	dt.Nuke( gc )

	fmt.Fprintf( os.Stderr, "[INFO] sketch: base graph sketch  test finished\n" )
}

/*
	Test a run graph.
*/
func Test_sketch_rungraph( t *testing.T )  {
	// xo,yo is upper left corner of the graph
	jstr := fmt.Sprintf( `{
			"Id": "rungraph1",
			"Kind": %d,
			"Xo":		10.0,
			"Yo":		50.0,
			"Visible": true,
			"Height":		250.0,
			"Width":		550.0,
			"Gheight":		100.0,
			"Gwidth":		480.0,
			"Orig_x":		50.0,
			"Orig_y":		230.0,
			"X_disp":		25,
			"Y_split":	4,
			"X_split":	8,
			"Yrmargin":	35,
			"Sp_colour":	"#200000",
			"Show_right_scale": false,
			"Label_size":	8,
			"Subtitle_size":	10,
			"Title_size":		12,
			"Title_colour":		"yellow",
			"Label_colour":	"white",
			"Title":	"Rungraph Lemming Births and Deaths",
			"Y_ltitle": "Lemming Deaths per Hour",
			"Y_rtitle": "Lemming Births per Hour",
			"Ymargin":		30,
			"Line_styles": [ 0 ],
			"Left_max":		15000,
			"Left_round":	1000,
			"Rot_angle": 0
		}`, DT_RUNGRAPH )

	jstr2 := fmt.Sprintf( `{
			"Id": "rungraph2",
			"Kind": %d,
			"Xo":		600.0,
			"Yo":		50.0,
			"Visible": true,
			"Height":		50.0,
			"Width":		100.0,
			"Gheight":		25.0,
			"Gwidth":		75.0,
			"Orig_x":		40.0,
			"Orig_y":		40.0,
			"X_disp":		0,
			"Y_split":	2,
			"X_split":	4,
			"Yrmargin":	35,
			"Ymargin":		30,
			"Sp_colour":	"#303030",
			"Show_right_scale": false,
			"Label_size":	8,
			"Subtitle_size":	6,
			"Title_size":		8,
			"Title_colour":		"yellow",
			"Label_colour":	"white",
			"Title":	"Small graph",
			"Y_ltitle": "Lemming Deaths per Hour",
			"Y_rtitle": "Lemming Births per Hour",
			"Line_styles": [ 0 ],
			"Left_max":		150,
			"Left_round":	10,
			"Rot_angle": 0
		}`, DT_RUNGRAPH )
	
	fmt.Fprintf( os.Stderr, "[INFO] run graph test starts\n" )
	dt, err := Json2ScaledDt( jstr )			// create a drawing thing (run graph) from json
	dt2, err := Json2ScaledDt( jstr2 )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: failed to crate run graph from json: %s\n", err )
		t.Fail()
		return
	}

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	gc.Select_page( "root" )
	draw_grid( gc, "#404040" )
	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Run Graph  Test" )
	title.Paint( gc, 1.0 )

	data := data_mgr.Mk_data_group( 2, 100, []string{ "white", "orange" } )
	data.Load_comms_sets( "sketch1.data", " ", 0, "1 2" )
	dt.Set_data( data )
	dt2.Set_data( data )
	dt2.Paint( gc, 1.0 )
	dt.Paint( gc, 1.0 )
	seq := data.Get_max_seq() + 1
	gc.Show( )	
	time.Sleep( 2 * time.Second )

	if ! gen_ps {
		for i := 0; i < 500; i++ {
			gc.Select_page( "root" )
			draw_grid( gc, "#a0a0a0" )

			dt.Paint( gc, 1.0 )
			dt2.Paint( gc, 1.0 )
			gc.Show( )	

			data.Add_sv( seq, float64( rand.Int() % 8000 ) -2000.00 )
			seq++
		
			if i % 5 == 1 {
				data.Select( 1 )
				data.Add_sv( 0, float64( rand.Int() % 10000 ) )
				data.Select( 0 )
			}

			//time.Sleep( 2 * time.Second )
			time.Sleep( 100 * time.Millisecond )
		}
	} else {
		dt.Paint( gc, 1.0 )
		gc.Show()
	}
	
	if ! gen_ps {
		time.Sleep( 5 * time.Second )
	}
	dt.Nuke( gc )

	jstr, err = dt.To_json()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] unable to generate json from run graph%s\n", err )
		t.Fail()
	} else {
		fmt.Fprintf( os.Stderr, "[EXAM] %s\n", jstr )
	}

	fmt.Fprintf( os.Stderr, "[INFO] sketch: run graph sketch  test finished\n" )
}

/*
	Test a bar graph.
*/
func Test_sketch_bargraph( t *testing.T )  {

	// NOTE: all variable values are placed at the top for eaasy maintenance
	jstr := fmt.Sprintf( `{
			"Kind": %d,
			"X_lab_style": %d,
			"Id": "bargraph1",
			"Xo":		100.0,
			"Yo":		50.0,
			"Visible": true,
			"Height":		500.0,
			"Width":		550.0,
			"Gheight":		400.0,
			"Gwidth":		375.0,
			"Orig_x":		50.0,
			"Orig_y":		450.0,
			"X_disp":		100,
			"Sp_colour":	"#200020",
			"Y_split":	4,
			"X_split":	5,
			"Yrmargin":	35,
			"Show_right_scale": false,
			"Label_size":	8,
			"Subtitle_size":	10,
			"Title_size":		12,
			"Title_colour":		"yellow",
			"Label_colour":	"white",
			"Title":	"Lemming Births And Violent Deaths",
			"Y_ltitle": "Lemming Deaths per Hour",
			"Y_rtitle": "Lemming Births per Hour",
			"Ymargin":		30,
			"Left_min":		260,
			"Left_round":	1000,
			"Left_max":		15000
		}`, DT_BARGRAPH, XL_REV_INDEX )

		
	fmt.Fprintf( os.Stderr, "[INFO] bar chart test starts\n" )
	dt, err := Json2ScaledDt( jstr )			// create a drawing thing (bar graph) from json

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: failed to crate bar graph from json: %s\n", err )
		t.Fail()
		return
	}
	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	gc.Select_page( "root" )
	draw_grid( gc, "#404040" )
	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Bar Graph  Test" )
	title.Paint( gc, 1.0 )

	data := data_mgr.Mk_data_group( 1, 75, []string{ "red", "orange" } )		// load some testing data; just one set with the last 75 values
	data.Load_comms_sets( "sketch1.data", " ", 0, "1 2" )						// ensure that giving more fields than the number of sets doesn't break
	data.Select( 0 )

	dt.Set_data( data )															// associate data with graph

	if ! gen_ps {
		for i := 0; i < 200; i ++ {
			dt.Paint( gc, 1.0 )
			gc.Show( )	

if i == 0 {
		time.Sleep( 3 * time.Second )
}
			data.Add_sv( 0, float64( rand.Int() % 10000 ) -2000  )
			//time.Sleep( 15 * time.Second )
			time.Sleep( 75 * time.Millisecond )
		}
	} else {
		dt.Paint( gc, 1.0 )
		gc.Show()
	}
	dt.Nuke( gc )

	jstr, err = dt.To_json()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] unable to generate json from bar graph%s\n", err )
		t.Fail()
	} else {
		fmt.Fprintf( os.Stderr, "[EXAM] %s\n", jstr )
	}

	fmt.Fprintf( os.Stderr, "[INFO] sketch: bar graph sketch  test finished\n" )
}

/*
	Test the meter code
*/
func Test_sketch_meter( t *testing.T )  {
	jstr := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Paint_style": %d,
			"Needle_style": %d,
			"Arc_type":	%d,
			"Radius": %d,
			"Tic_marks": true,
			"Text_size": 12,
			"Text_colour": "yellow",
			"Id": "test_sketch_meter", 
			"Xo":	450, 
			"Yo":	300, 
			"Val_min": 0,
			"Val_max": 2000,
			"Visible": true }`, DT_METER, Paint_jump, Needle_full, Meter_220, 50 )

	jstr2 := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Paint_style": %d,
			"Needle_style": %d,
			"Arc_type":	%d,
			"Radius": %d,
			"Tic_marks": true,
			"Text_size": 10,
			"Text_colour": "yellow",
			"Id": "test_sketch_meter", 
			"Xo":	300, 
			"Yo":	300, 
			"Val_min": 0,
			"Val_max": 100,
			"Visible": true }`, DT_METER, Paint_jump, Needle_tic, Meter_180, 50 )

	jstr3 := fmt.Sprintf( 
		`{ "Kind": %d, 
			"Paint_style": %d,
			"Needle_style": %d,
			"Center_box": true,
			"Arc_type":	%d,
			"Radius": %d,
			"Tic_marks": true,
			"Text_size": 10,
			"Text_colour": "green",
			"Id": "test_sketch_meter", 
			"Xo":	100, 
			"Yo":	300, 
			"Val_min": 0,
			"Val_max": 1.0,
			"Visible": true }`, DT_METER, Paint_jump, Needle_tic, Meter_90, 50 )

	dt, err := Json2ScaledDt( jstr )
	dt2, err := Json2ScaledDt( jstr2 )
	dt3, err := Json2ScaledDt( jstr3, 1.0 )

	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create dt from base json: %s\n",err ) 
		t.Fail()
		return
	}

	if dt == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: returned pointer to meter was nil!\n" )
		t.Fail()
		return
	}

	fmt.Fprintf( os.Stderr, "[OK]   created meter dt from base json: %s\n", dt ) 

	gc, err := get_gc()
	if err != nil {
		fmt.Fprintf( os.Stderr, "[FAIL] sketch test: unable to create graphics construct\n" )
		t.Fail()
		return
	}

	title, _ := mk_txt_dt( 20, 20, "white", 14, "Sans", "Meter Test" )
	title.Paint( gc, 1.0 )

	dt.Set_data( float64( 1500.0 )  )
	dt2.Set_data( float64( 99.0 )  )
	dt3.Set_data( float64( .70 )  )

	for i := 0; i < 100; i++ {
		dt.Paint( gc, 1.0 )
		dt2.Paint( gc, 1.0 )
		dt3.Paint( gc, 1.0 )

		gc.Show( )
		time.Sleep( 100 * time.Millisecond )
	}

	//time.Sleep( 2000 * time.Millisecond )

	dt.Set_data( float64( 800.0 )  )
	for i := 0; i < 100; i++ {
		dt.Paint( gc, 1.0 )
		dt2.Paint( gc, 1.0 )
		dt3.Paint( gc, 1.0 )

		gc.Show( )
		time.Sleep( 100 * time.Millisecond )
	}

	time.Sleep( 15 * time.Second )
}


// ---------------- this must be last -----------------

func Test_wrap_up( t *testing.T )  {
	gc, err := get_gc( ) 
	if err == nil {
		gc.Show()
		gc.Close() 
	}
}
