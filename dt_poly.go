
/*
	Mnemonic:	dt_poly.go
	Abstract:	A regular polygon described by a center origin, radius and number
				of sides.
	Date:		30 May 2018
	Author:		Scott Daniels
*/

package drawingthings

import (
	"fmt"
	"encoding/json"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

type Dt_reg_polygon struct {
	Dt_base;		// common fields and functions

	Fill_style int
	Fcolour string		// fill colour

	Points	[]*sktools.Point
	Radius	float64
	Sides	int
	Line_style	int
}

// ---- private things ---------------------------------------------------------------------------------
/*
	Mk_points computes the slice of points that can be givn to the underlying gc draw pologyn
	function for drawing. Because xo,yo is the centre of the polygon, we must adjsut each point.
*/
func ( dt *Dt_reg_polygon ) mk_points( ) {
	angle := 360.0 / float64( dt.Sides )			// angle of each vertex
	
	pts := make( []*sktools.Point, dt.Sides + 1 )
	idx := 0;
	for cangle := float64( 0 ); cangle < 360; cangle += angle {
		pts[idx] = Circle_pt(  dt.Scale, dt.Xo, dt.Yo, dt.Radius, cangle + dt.Rot_angle )		// we can rotate it without gc help
		idx++
	}
	pts[idx] = pts[0]
	
	dt.Points = pts
}

// -----------------------------------------------------------------------------------------------------


/*
	Mk_def_poly creates a polygon struct with default (less than useful) values.
*/
func Mk_def_poly( ) ( dt *Dt_reg_polygon ) {
	dt = &Dt_reg_polygon{
		Dt_base: Dt_base {
			Kind:	DT_REGPOLY,
			Scale:	1.0,
			Visible: true,
		},

		Sides:	5,
		Radius:	100,
	}

	return dt
}

/*
	Mk_rect allows an instance of the regular poly struct to be created directly rather than 
	creating a generic drawing thing.
*/
func Mk_reg_poly( xo float64, yo float64, radius float64, sides int, outline_colour string, fill bool, fill_colour string ) ( *Dt_reg_polygon ) {
	if sides < 5 {
		sides = 5
	} else {
		if sides > 100 {
			sides = 100
		}
	}

	dt := &Dt_reg_polygon{
		Dt_base: Dt_base {
				Xo:	xo,
				Yo: yo,
				Kind:	DT_REGPOLY,
				Colour: outline_colour,
				Scale:	1.0,
				Visible: true,
		},

		Sides:	sides,
		Radius:	radius,
		Fcolour: fill_colour,
	}


	if fill {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}

	dt.mk_points( )			// create now  so no hit at first paint
	return dt
}

/*
	Paint draws the polygon according to the information in the struct.
*/
func ( dt *Dt_reg_polygon ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil || ! dt.Is_visible( scale ) {
		return
	}

	if dt.Points == nil  {
		dt.mk_points(  )		// will rotate the thing if angle != 0
	}

	outline := false
	if dt.Colour != "none" {				// select outline if colour is given
		gc.Set_colour( dt.Colour )
		outline = true
	}

	pattern := Line_style_to_pattern( dt.Line_style )
	if pattern != nil {				// any line style other than solid has a pattern and must be set
		gc.Push_state()
		defer gc.Pop_state( ) 
		gc.Set_line_style( pattern )
	}

	gc.Set_fill_attrs( dt.Fcolour, dt.Fill_style )
	gc.Draw_poly( dt.Points, outline )
}

/*
	Rotate captures the information needed to rotate the triangle when painted.
	The rotation point is given as a vertex number (1, 2, 3 etc.) and to rotate
	round the centre of the triangle, the special vertex of 0 is used.

	Some drawing things might treat a vertex of 1.5 to mean halfway between the first
	and second vertex, and others might ignore the fractional part all together.
*/
func ( dt *Dt_reg_polygon ) Rotate( vertex float64, angle float64 ) {
	if dt == nil {
		return
	}

	dt.Rot_angle = angle
	dt.Rot_vertex = vertex
	dt.mk_points( )				// must make points as rotated
}

/*
	Set a new orign for the drawing thing. 
*/
func (dt *Dt_reg_polygon) Set_origin(new_xo float64, new_yo float64) {
	if dt == nil {
		return
	}

	dt.Xo = new_xo
	dt.Yo = new_yo
	dt.mk_points( )		// must remake points
}

/*
	Set the fill value.  If true passed, then rectangle will be filled
*/
func ( dt *Dt_reg_polygon ) Set_fill(  state bool ) {
	if dt == nil {
		return
	}

	if state {
		dt.Fill_style = sketch.FILL_SOLID
	} else {
		dt.Fill_style = sketch.FILL_NONE
	}
}

/*
	Set_line_style allows the style of the outline to be changed
	after the thing has been created. Expects one of the *_LINE
	constants.
*/
func ( dt *Dt_reg_polygon ) Set_line_style(  style int ) {
	if dt == nil {
		return
	}

	dt.Line_style = style 
}

/*
	Set the fill colour to use when the rectangle fill state is true.
*/
func ( dt *Dt_reg_polygon ) Set_fill_colour(  cname string ) {
	if dt != nil {
		dt.Fcolour = cname
	}
}

/*
	Set_iscale will set the drawing scale of the poligyon by adding
	the increment passed in to the current value.
*/
func ( dt *Dt_reg_polygon ) Set_iscale( increment float64 ) {
	if dt != nil {
		dt.Scale += increment
		dt.Points = nil				// force recompute on next paint
	}
}

/*
	Set_scale will set the drawing scale of the poligyon to the value 
	passed in. 
*/
func ( dt *Dt_reg_polygon ) Set_scale( new_scale float64 ) {
	if dt != nil {
		dt.Scale = new_scale
		dt.Points = nil				// force recompute on next paint
	}
}

/*
	To_json will generate a json string from this struct.
*/
func ( dt *Dt_reg_polygon ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	hold_points := dt.Points
	dt.Points = nil

	jbytes, err := json.Marshal( dt )

	dt.Points = hold_points

	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}

func ( dt *Dt_reg_polygon ) String( ) ( string ) {
	if dt != nil {
		return fmt.Sprintf( "dt: kind=%d id=%s orig=(%.2f,%.2f) colour=%s filled=%v fcolour=%s vis=%v", 
			dt.Kind, dt.Id, dt.Xo, dt.Yo, dt.Colour, dt.Fill_style == sketch.FILL_SOLID, dt.Fcolour, dt.Visible )
	}

	return "<nil>"
}
