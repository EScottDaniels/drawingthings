// :vi ts=4 sw=4 noet :
/*
	Mnemonic:	dt_bar_graph.go
	Abstract:	The struct and functions needed to implement a bar graph.
	Date:		22 May 2018
	Author:		E. Scott Daniels
*/

package drawingthings

import (
	"encoding/json"
	"math"

	"bitbucket.org/EScottDaniels/sketch"
)

const (
	DT_BGSTYLE_SOLID	int = iota
	DT_BGSTYLE_TRANSP
	DT_BGSTYLE_OUTLINE
)

type Dt_bar_graph struct {
	Dt_graph_base

	style	int			// solid, transparent, outline
}

/*
	This generates a bar graph struct with defaults.
*/
func Mk_default_bar_graph( ) ( *Dt_bar_graph ) {

	height := 150.0

	dt := &Dt_bar_graph {
		Dt_graph_base: Dt_graph_base {
			Dt_base: Dt_base {
				Kind: DT_BARGRAPH,
				Xo:		0.0,			// lower left of the sub page
				Yo:		0.0,
				Visible: true,
			},

			Height:		height,
			Width:		250.0,
			Gheight:	100.0,
			Gwidth:		200.0,
			Orig_x:		30.0,				// origin offset in sub page
			Orig_y:		height + 30.0,
			Y_split:	4,
			X_split:	4,
			Label_size:	8,
			Ymargin:		30,
			Yrmargin:		0,

			Subtitle_size:	10,
			Title_size:		12,
			Title_font:		"Sans",
			Subtitle_font: 	"Sans",
			Title_colour:	"white",
			Subtitle_colour:	"white",

			Data:		nil,
			
			Grid_colour: "#909090", 		// colour for the grid
			Axis_colour:	"white",		// colour for the axis
			Bg_colour:	"#202020",			// colour for the plote area background
			Sp_colour:	"#000000",			// colour for the sub page

			Scale_fmt:	"%.0f",
			Show_xgrid:	true,
			Show_ygrid:	true,
			Show_left_scale: true,	// show the scale on the left axis
			Show_right_scale: false,	// show the scale on the right axis
			Show_xlabels: true,
			Show_left_yaxis: true,	// allow painting of either y axis
			Show_right_yaxis: false,
			
			Right_min:	-1.0,		// use values from data if <0
			Right_max:	-1.0,		// top value for right y axis
			Left_min:	-1.0,		// x-axis value for left y axis
			Left_max:	-1.0,		// top value for left y axis
			Left_round:	-1.0,
		},

		style:		DT_BGSTYLE_SOLID,
	}

	return dt
}

/*
	draw_bars draws the bars corresponding to the data. The assumption is that
	the graphics page's origin is at the top left.
*/
func ( dt *Dt_bar_graph ) draw_bars( gc sketch.Graphics_api, min float64, max float64  ) {
	var ( 
		b Drawingthing_i
	)

	if dt == nil || dt.Data == nil {
		return
	}

	dg := dt.Data									// convenience
	bwidth := (dt.Gwidth - 2) / float64( dg.Get_set_size() )
	yo := dt.Orig_y - dt.X_disp

	for set := 0; set < dg.Size(); set++ {			// for each set
		x := dt.Orig_x + 1
		colour := dg.Get_set_colour( set )			// this set of bar colours
		values := dg.Get_nvalues( set, min, max )	// values normalised 0.0 .. 1.0

		if bwidth < 2 {
			gc.Set_colour( colour )
		}

		for _, v := range values {
			if bwidth >= 2 {
				if v < 0 {
					v = math.Abs( v )
					b = Mk_rect( x, yo+1, (dt.Gheight * v),  bwidth, "black", true, colour )
				} else {
					b = Mk_rect( x, (yo-(dt.Gheight * v)) - 1, (dt.Gheight * v),  bwidth, "black", true, colour )
				}
			} else {	
				if v < 0 {
					b = Mk_line( x, yo-(dt.Gheight * v), x, yo+1, colour, SOLID_LINE )
				} else {
					b = Mk_line( x, (yo-(dt.Gheight * v)) - 1, x, yo-1, colour, SOLID_LINE )
				}
			}

			b.Paint( gc, 0.0 )				// scale is fixed because we always paint theses
			x += bwidth
		}
	}
}

/*
	Paint will render the bar graph using the graphics context (gc) passed in.
*/
func ( dt *Dt_bar_graph ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if dt == nil {
		return
	}

	gc.Push_state( )
	defer gc.Pop_state()

	if dt.Rot_angle != 0 {
		gc.Push_state()
		defer gc.Pop_state( )
		gc.Translate_delta( dt.Xo + (dt.Width/2), dt.Yo + (dt.Height/2) )		// rotate on the centre of the drawing area
		gc.Rotate( dt.Rot_angle )
		gc.Translate_delta( -(dt.Xo + (dt.Width/2)), -(dt.Yo + (dt.Height/2)) )		// return orign to expected location
	}

	min, max := dt.paint_underlay( gc )				// create a subpage and draw desired grids etc

	dt.draw_bars( gc, min, max )
}


/*
	Return json from the underlying drawing thing.
*/
func ( dt *Dt_bar_graph ) To_json( ) ( string, error ) {
	if dt == nil {
		return "{}", nil
	}

	jbytes, err := json.Marshal( dt )
	if err == nil {
		return string( jbytes ), nil
	} else {
		return "", err
	}
}
