
package drawingthings

import (
	"fmt"
	"os"
	"math"
	"testing"

	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

func check( x float64, y float64, desiredx float64, desiredy float64,  tcase string, t *testing.T ) {
	if x - desiredx > math.Abs( .001 )  {
		t.Fail()
		fmt.Fprintf( os.Stderr, " circle_pt did not return expected X value for %s: x=%.9f desired=%.9f\n", tcase, x, desiredx )
	}

	if y - desiredy > math.Abs( .001 )  {
		t.Fail()
		fmt.Fprintf( os.Stderr, " circle_pt did not return expected Y value for %s: y=%.9f desired=%.9f\n", tcase, y, desiredy )
	}
}

func Test_tools( t *testing.T )  {
	x, y := Circle_pt( 1.0, 0, 0,  1, 45 ).Get_xy() 
	check( x, y, y, x, "45 degrees", t )

	x, y = Circle_pt( 1.0, 0, 0,  1, 90 ).Get_xy() 
	check( x, y, 0.0, 1.0, "90 degrees", t )

	x, y = Circle_pt( 1.0, 0, 0,  1, 0 ).Get_xy() 
	check( x, y, 1.0, 0.0, "0 degrees", t )
}

func Test_rotation( t *testing.T ) {
	
	rpt := sktools.Mk_point( 0, 0 )

	pts := make( []*sktools.Point, 8 )
	pts[0] = sktools.Mk_point( 1, 0 )
	pts[1] = sktools.Mk_point( 1, 1 )
	pts[2] = sktools.Mk_point( 0, 1 )
	pts[3] = sktools.Mk_point( -1, 1 )
	pts[4] = sktools.Mk_point( -1, 0 )
	pts[5] = sktools.Mk_point( -1, -1 )
	pts[6] = sktools.Mk_point( 0, -1 )
	pts[7] = sktools.Mk_point( 1, -1 )

	np := Rotate_pts( rpt, pts, 10.0 ) //.Point, points []*sktools.Point, angle float64 ) ( new_points []*sktools.Point ) {

	for i := 0; i < 6; i++ {
		x, y := np[i].Get_xy()
		fmt.Fprintf( os.Stderr, "rotation: (%.2f, %2f)\n", x, y )
	}
}
